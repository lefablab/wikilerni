// -- GESTION DE LA PAGE DU FINANCEMENT PARTICIPATIF

/// Permet d'afficher les boutons de paiement WP suivant le choix de l'utilisateur.

// Fonctions utiles au script :
import { helloDev } from "./tools/everywhere.js";
import { loadMatomo } from "./tools/matomo.js";
import { showBtnPayment, unCheckAllOthers } from "./tools/webportage.js";

helloDev();
loadMatomo();

const initialise = async () =>
{
    try
    {
        // Les boutons de paiement WP :        
        const CGV=document.getElementById("CGVOk");
        const abo1=document.getElementById("abo1");
        const abo2=document.getElementById("abo2");
        const abo3=document.getElementById("abo3");
        const abo4=document.getElementById("abo4");
        const divWPBtns=document.getElementById("WPBtns");
        divWPBtns.style.display="none";
        abo1.addEventListener("change", function(e)
        {
            unCheckAllOthers("abo1");
        });
        abo2.addEventListener("change", function(e)
        {
            unCheckAllOthers("abo2");
        });
        abo3.addEventListener("change", function(e)
        {
            unCheckAllOthers("abo3");
        });
        abo4.addEventListener("change", function(e)
        {
            unCheckAllOthers("abo4");
        });
        CGV.addEventListener("change", function(e)
        {
            if(CGV.checked===true)
            {
                divWPBtns.style.display="block";
                if(abo1.checked===true)
                    showBtnPayment("btn1");
                else if(abo2.checked===true)
                    showBtnPayment("btn2");
                else if(abo3.checked===true)
                    showBtnPayment("btn3");
                else if(abo4.checked===true)
                    showBtnPayment("btn4");
                else
                {
                    divWPBtns.style.display="none";
                    CGV.checked=false;
                }
            }
            else
                divWPBtns.style.display="none";
        });
    }
    catch(e)
    {
        console.error(e);
    }
}
initialise();