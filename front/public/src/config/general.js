// à supprimer une fois que tout récupérer du backend :

module.exports =
{
    apiUrl : "http://localhost:3000/api",
    usersGetConfigUrl : "/user/getconfig",
    lang : "fr",
    userHomePage : "accueil.html",
    adminHomePage : "admin.html",
    managerHomePage : "gestion.html",
    subscribePage : "inscription.html",
    connectionPage : "connexion.html",
    accountPage: "compte.html",
    questionnairesManagementPage: "gestion-quizs.html",
    usersManagementPage: "gestion-utilisateurs.html",
    nbQuestionnairesUserHomePage : 10,
    illustrationDir : "/img/quizs/"
};