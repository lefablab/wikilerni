// -- GESTION DE LA PAGE PERMETTANT DE LISTER LES QUIZS AUXQUELS L'UTILISATEUR A DÉJÀ RÉPONDU

/// Il est d'abord testé que le navigateur accepte l'utilisation d'IndexDB
/// Si oui la liste de quizs ayant déjà reçu au moins une réponse est affichée.
/// Un bouton permettra aussi de les sauvegarder dans un fichier ou d'importer une sauvegarde.

// Configurations générales provenant du backend :
import { availableLangs } from "../../../config/instance.js";
const lang=availableLangs[0];

// Textes :
const {  localDBNotReady, localFileFail, localFileImportOK, wantToSaveResultsDatas }=require("../../../lang/"+lang+"/answer");
const { serverError }=require("../../../lang/"+lang+"/general");

// Fonctions :
import { addElement } from "./tools/dom.js";
import { helloDev } from "./tools/everywhere.js";
import { isEmpty } from "../../../tools/main";
import { loadMatomo } from "./tools/matomo.js";

// Classe s'occupant du stockage des résultats aux quizs :
import { userQuizsResults} from "./tools/userQuizsResults";

// Éléments du DOM manipulés :
const datas2Restore=document.getElementById("datas2Restore");
const quizsList=document.getElementById("quizsList");
const responseTxt=document.getElementById("response");

let userDB, allPreviousAnswers=[], myResults;
const initialise = async () =>
{
    try
    {
        // Instanciation de la classe s'occupant du stockage des résultats aux quizs :
        myResults=await userQuizsResults.initialise("myResults", 2);
        // Si la base de données est fonctionnel et que des résultats sont déjà enregistrés, on affiche la liste des quizs ayant une réponse connue :
        if(myResults.dbIsReady !== false)
        {
            myResults.showMyQuizs();
            if(myResults.allResults.length !== 0)
                myResults.saveMyQuizs("quizsSave", wantToSaveResultsDatas, ["button"]);
        }
        else
            addElement(quizsList, "p",  localDBNotReady);
        // Statistiques :
        loadMatomo();
    }
    catch(e)
    {
        console.error(e);
    }
}
initialise();
helloDev();

datas2Restore.addEventListener("change", function(e)
{
    try
    {
        responseTxt.innerHTML="";
        const selectedFiles=datas2Restore.files;
        if(selectedFiles !== null && selectedFiles.length === 1)
        {
            // selectedFiles[0].type ne fonctionne pas avec certains navigateurs (Fennec), donc... :
            const extension=selectedFiles[0].name.substring(selectedFiles[0].name.lastIndexOf(".")+1);
            if(extension !== "json")
               addElement(responseTxt, "p", localFileFail, "", ["error"]);
            else
            {
                // Lecture du contenu du fichier qui est passé au parseur :
                const reader=new FileReader();
                reader.onload=async function(e)
                {
                    const datas=JSON.parse(reader.result);
                    if(myResults.dbIsReady !== false)
                    {
                        if((datas.quizs.length !==0) &&(datas.results.length !==0))
                        {
                            await myResults.saveAllResults(datas.results);
                            await myResults.saveAllQuizs(datas.quizs);
                            // Puis actualise l'affichage :
                            myResults.showMyQuizs();
                            addElement(responseTxt, "p", localFileImportOK, "", ["success"]);
                        }
                    }
                };
                reader.readAsText(selectedFiles[0]);
            }
        }
    }
    catch(e)
    {
        console.error(e);
    }
});