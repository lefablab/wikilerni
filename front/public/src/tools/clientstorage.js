// FONCTIONS UTILES AU STOCKAGE LOCAL (SESSION, COOKIES, INDEXDB, ETC.)
// Éviter sessionStorage dont le contenu n'est pas gardé d'un onglet à l'autre : https://developer.mozilla.org/fr/docs/Web/API/Window/sessionStorage

export const saveLocaly = (name, data) =>
{
    localStorage.setItem(name, JSON.stringify(data));
}

export const getLocaly = (name, json=false) =>
{
    if(json)
        return JSON.parse(localStorage.getItem(name));
    else
        return localStorage.getItem(name);
}

export const removeLocaly = (name) =>
{
    localStorage.removeItem(name);
}

export const saveIsReady = () =>
{
    if (!window.indexedDB)
        return false;
    else
        return true;
}

export const getStore = (db, store_name, mode) =>
  {
    const tx=db.transaction(store_name, mode);
    return tx.objectStore(store_name);
  }