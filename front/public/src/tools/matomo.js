import { matomo } from "../../../../config/matomo.js";

// Fonction chargeant le code matomo
export const loadMatomo = () =>
{
    // chargement de matomo
    var _paq = window._paq = window._paq || [];
    //ajout cnil  :
    _paq.push([function()
    {
        var self = this;
        function getOriginalVisitorCookieTimeout()
        {
            var now = new Date(),
            nowTs = Math.round(now.getTime() / 1000),
            visitorInfo = self.getVisitorInfo();
            var createTs = parseInt(visitorInfo[2]);
            var cookieTimeout = 33696000; // 13 mois en secondes
            var originalTimeout = createTs + cookieTimeout - nowTs;
            return originalTimeout;
        }
        this.setVisitorCookieTimeout( getOriginalVisitorCookieTimeout() );
    }]);
    //--fin ajout cnil
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u=matomo.url;
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', matomo.siteId]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
}