// -- SCRIPT DE BASE APPELÉ DANS LES PAGES TYPE ACCUEIL DU SITE
import { helloDev } from "./tools/everywhere.js";
import { loadMatomo } from "./tools/matomo.js";

helloDev();
loadMatomo();