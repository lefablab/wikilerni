const path = require('path');

module.exports =
{
  mode: "development",
  entry:
  {
    index: "./src/index.js",
    myQuizs: "./src/myQuizs",
    paymentPage: "./src/paymentPage.js",
    polyfill: "babel-polyfill",
    quiz: "./src/quiz.js"
  },
  output:
  {
    path: path.resolve(__dirname, "www"),
    filename: "JS/[name].app.js",
  },
  module:
  {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  devServer:
  {
    static: path.resolve(__dirname, "./www")
  },
};