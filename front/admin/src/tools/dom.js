import { isEmpty } from "../../../../tools/main";

// Fonction associant les attributs fournis à un champ de formulaire
export const addElement = (eltParent, eltType, eltContent="", eltId="", eltClass=[], eltAttributes={}, replace=true) =>
{
    if(isEmpty(eltType) || isEmpty(eltParent))
        return false;
    else
    {
        const newElement=document.createElement(eltType);
        
        if(!isEmpty(eltId))// tester si l'id n'est pas déjà utilisé dans le DOM ?
            newElement.id=eltId;

        if(Array.isArray(eltClass) && eltClass.length!=0)
        {
            for(let i in eltClass)
                newElement.classList.add(eltClass[i]);
        }

        if(typeof eltAttributes === "object") // !! tous les objets ne sont pas ok
        {
            for(let attributName in eltAttributes)
                newElement.setAttribute(attributName, eltAttributes[attributName]);
        }

        if(!isEmpty(eltContent))
            newElement.innerHTML=eltContent.replace(/\n/g,"<br>");// innerHTML permet d'ajouter du texte ayant lui-même des balises, etc.
            
        if(replace)
            eltParent.innerHTML="";
        eltParent.appendChild(newElement);
    }    
}