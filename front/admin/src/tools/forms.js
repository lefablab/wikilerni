import { isEmpty } from "../../../../tools/main";

// Fonction associant les attributs fournis à un champ de formulaire
export const setAttributesToInputs = (inputsConf, myForm) =>
{
    for(let i in myForm.elements)
    {
        if(!isEmpty(myForm.elements[i].id))
        {
            let idInput=myForm.elements[i].id;
            if(inputsConf[idInput]!==undefined)
            {
                let inputHTML=document.getElementById(idInput);
                for (let attribute in inputsConf[idInput])
                    inputHTML.setAttribute(attribute, inputsConf[idInput][attribute]);
            }
        }
    }
    return true;
}

// Récupère toutes les valeurs de champs en omettant les checkbox non cochées, etc.
export const getDatasFromInputs = (myForm) =>
{
    const datas={};
    const formData = new FormData(myForm);
    for(let entrie of formData.entries())
       datas[entrie[0]]=entrie[1];
    return datas;
}

// Vide tous les champs d'un formulaire, y compris hidden, checkbox, etc.
// Revoir pour les select
export const empyForm = (myForm) =>
{
    const formData = new FormData(myForm);
    for(let entrie of formData.entries())
    {
        if(myForm.elements[entrie[0]].type=="checkbox" || myForm.elements[entrie[0]].type=="radio")
            myForm.elements[entrie[0]].checked=false;
        else
            myForm.elements[entrie[0]].value="";
    }
    return true;
}
// Vide et cache le formulaire
export const empyAndHideForm = (myForm) =>
{
    empyForm(myForm);
    myForm.style.display="none";
}