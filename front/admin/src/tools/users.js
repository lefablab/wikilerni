import { apiUrl, availableLangs, siteUrl, theme } from "../../../../config/instance.js";
const lang=availableLangs[0];

const configTemplate = require("../../../../views/"+theme+"/config/"+lang+".js");

import {  checkLoginRoute, timeDifferenceMax, timeDifferenceMin, userRoutes } from "../../../../config/users.js";

import { getLocaly, removeLocaly, saveLocaly } from "../tools/clientstorage.js";
import { isEmpty } from "../../../../tools/main";

export const getTimeDifference = () =>
{
    // multiplier par -1, car c'est ce qu'il faut "ajouter" à l'heure UTC pour revenir en heure locale qui m'intéresse et non l'inverse
    const timeLocal=new Date().getTimezoneOffset()*-1;
    if(timeLocal > timeDifferenceMax || timeLocal < timeDifferenceMin)
        return 0;
    else
        return timeLocal;
}

// J'utilise le stockage local du navigateur pour enregistrer les données permettant de reconnaître l'utilisateur par la suite
// Seul le serveur pourra vérifier que les identifiants sont (toujours) valides.
export const setSession = (userId, token, durationTS) =>
{
    const storageUser=
    {
        id: userId,
        token: token,
        duration: durationTS
    }
    saveLocaly("user", storageUser);
}

// Vérifie qu'il y a des données locales concernant le résultat d'un quiz ou d'un groupe de quizs
// Et les ajoute aux données envoyées par les formulaires d'inscription/connexion si c'est le cas
export const checkAnswerDatas = (datas) =>
{
    const lastAnswer=getLocaly("lastAnswer");
    if(!isEmpty(lastAnswer))
    {
        const answer=JSON.parse(lastAnswer);
        if(!isEmpty(answer.duration) && !isEmpty(answer.nbCorrectAnswers) && !isEmpty(answer.nbQuestions) && (!isEmpty(answer.QuestionnaireId) || !isEmpty(answer.GroupId)))
        {
            datas.duration=answer.duration;
            datas.nbCorrectAnswers=answer.nbCorrectAnswers;
            datas.nbQuestions=answer.nbQuestions;
            if(!isEmpty(answer.QuestionnaireId))
                datas.QuestionnaireId=answer.QuestionnaireId;
            else
                datas.GroupId=answer.GroupId;
        }
    }
    return datas;
}


/// La suite est toujours utile pour l'admin.

// Cette fonction teste la connexion de l'utilisateur d'une page
// On peut fournis une liste de statuts acceptés (si vide = tous), ainsi qu'une url de redirection si non connecté, un message d'erreur à afficher sur la page de destination et l'url sur laquelle revenir une fois connecté
export const checkSession = async (status=[], urlRedirection, message, urlWanted) =>
{
    return new Promise((resolve, reject) =>
    {
        const userDatas=getLocaly("user");
        if(isEmpty(userDatas))
        {
            redirectUser(urlRedirection, message, urlWanted);
            resolve(false);
        }
        else
        {
            const user=JSON.parse(userDatas);
            if(isEmpty(user.id) || isEmpty(user.token) || isEmpty(user.duration) || user.duration < Date.now())
            {
                removeLocaly("user");
                redirectUser(urlRedirection, message, urlWanted);
                resolve(false);
            }
            else
            {
                const xhr = new XMLHttpRequest();
                xhr.open("GET", apiUrl+userRoutes+checkLoginRoute+user.token);
                xhr.onload = () =>
                {
                    let response=JSON.parse(xhr.responseText);
                    if (xhr.status === 200 && response.isValid && response.id != undefined)
                    {
                        if(response.id===user.id)
                        {
                            user.name=response.name;
                            user.language=response.language;
                            user.timeDifference=response.timeDifference;
                            user.status=response.status;// c'est le token qui sert à vérifier le statut à chaque requête à l'API
                            saveLocaly("user", user);
                            // si il s'agit d'un "user" et que son abonnement a expiré, je le redirige vers la caisse :-)
                            if(response.status==="user" && response.nbDaysOk <= 0)
                            {
                                const urlAccount=siteUrl+"/"+configTemplate.accountPage;
                                if(window.location.href.indexOf(urlAccount)===-1)
                                    window.location.assign("/"+configTemplate.accountPage+"#subscribe");// passée directement ici, l'ancre #subscribe ne fonctionne pas !?
                                resolve(true);
                            }
                            else
                            {
                                if(status.length!==0 && status.indexOf(response.status)===-1)
                                {
                                    redirectUser(urlRedirection, message, urlWanted);
                                    resolve(false);
                                }
                                else
                                    resolve(true);
                            }
                        }
                        else
                        {
                            removeLocaly("user");
                            redirectUser(urlRedirection, message, urlWanted);
                            resolve(false);
                        }
                    }
                    else
                    {
                        removeLocaly("user");
                        redirectUser(urlRedirection, message, urlWanted);
                        resolve(false);
                    }
                }
                xhr.onerror = () => reject(xhr.statusText);
                xhr.send();
            }
        }
    });
}
// Cette fonction sert à la précédente en cas de connexion non valide
const redirectUser = (urlRedirection, message, urlWanted) =>
{
    if(!isEmpty(message))
        saveLocaly("message", message);
    if(!isEmpty(urlWanted))
        saveLocaly("url", urlWanted);
    if(!isEmpty(urlRedirection))
        window.location.assign(urlRedirection);
}