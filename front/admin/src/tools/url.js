import { isEmpty } from "../../../../tools/main";

// Fonction récupérant les paramètres passés par l'url
export const getUrlParams = () =>
{
    if(isEmpty(location.search))
        return false;

    const parameters = location.search.substring(1).split("&");
    if(!Array.isArray(parameters) || parameters.length===0)
        return false;
        
    let param, datas={};
    for(let i in parameters)
    {
        param = parameters[i].split("=");
        if(param.length===2)
            datas[param[0]]=decodeURI(param[1]);
    }
    return datas;
}