// Ce script fournit des fonctions utilisées sur toutes les pages du site
export const helloDev = () =>
{
    console.log("**** Hello les devs :-)\nLe code source de WikiLerni est libre et vous pouvez le trouver à cette adresse :\nhttps://forge.chapril.org/Fab_Blab/WikiLerni\nBonne lecture ! ****");
    return true;
}