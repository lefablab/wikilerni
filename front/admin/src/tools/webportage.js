const checkBoxes=
{
    "CGV" : document.getElementById("CGVOk"),
    "abo1" : document.getElementById("abo1"),
    "abo2" : document.getElementById("abo2"),
    "abo3" : document.getElementById("abo3"),
    "abo4" : document.getElementById("abo4")
}
const divWPBtns=document.getElementById("WPBtns");

// Lorsque l'on sélectionne un montant, les autres options + les CGV sont désélectionnés
export const unCheckAllOthers = (choice) =>
{
    for (let id in checkBoxes)
    {
        if(id!==choice)
            checkBoxes[id].checked=false;
        divWPBtns.style.display="none";
    }
}


const btns=
{
    "btn1" : document.getElementById("WPBtn1"),
    "btn2" : document.getElementById("WPBtn2"),
    "btn3" : document.getElementById("WPBtn3"),
    "btn4" : document.getElementById("WPBtn4")
}

// Affiche le bon bouton de paiement et cache les autres
export const showBtnPayment = (choice) =>
{
    for (let id in btns)
    {
        if(id!==choice)
            btns[id].style.display="none";
        else
            btns[id].style.display="block";
    }
}