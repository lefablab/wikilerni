// -- GESTION DU FORMULAIRE PERMETTANT DE SAISIR / ÉDITER LES INFOS DES GROUPES DE QUIZS

/// Vérifie que l'utilisateur est bien connecté, a le bon statut et le redirige vers le formulaire d'inscription si ce n'est pas le cas.
/// Si c'est ok, propose un moteur de recherche permettant de chercher un groupe.
/// Si un id est passé par l'url on affiche les informations du groupe dans un formulaire permettant de l'éditer/supprimer.
/// Si pas d'id passé par l'url, on affiche un formulaire vide permettant d'en saisir un nouveau.

// Fichiers de configuration :
import { apiUrl, availableLangs, siteUrl, theme } from "../../../config/instance.js";
const lang=availableLangs[0];
const configQuestionnaires = require("../../../config/questionnaires.js");
const configTemplate = require("../../../views/"+theme+"/config/"+lang+".js");

// Fonctions :
import { getLocaly, removeLocaly } from "./tools/clientstorage.js";
import { addElement } from "./tools/dom.js";
import { helloDev } from "./tools/everywhere.js";
import { empyForm, getDatasFromInputs, setAttributesToInputs } from "./tools/forms.js";
import { dateFormat, isEmpty, replaceAll } from "../../../tools/main";
import { getUrlParams } from "./tools/url.js";
import { checkSession } from "./tools/users.js";

// Dictionnaires :
const { addOkMessage, serverError } = require("../../../lang/"+lang+"/general");
const { groupQuestionnairesList, groupQuestionnairesListWithout, infosGroupForAdmin, searchWithoutResult } = require("../../../lang/"+lang+"/group");
const { needBeConnected } = require("../../../lang/"+lang+"/user");

// Principaux éléments du DOM manipulés :
const btnNewGroup = document.getElementById("wantNewGroup");
const btnPreviewGroup = document.getElementById("previewGroup");
const deleteCheckBox = document.getElementById("deleteOkLabel");
const divCrash = document.getElementById("crash");
const divGroupIntro = document.getElementById("groupIntro");
const divMain = document.getElementById("main-content");
const divMessage = document.getElementById("message");
const divQuestionnaires = document.getElementById("questionnairesList");
const divResponse = document.getElementById("response");
const divSearchResult = document.getElementById("searchResult");
const formGroup = document.getElementById("groups");
const formSearch = document.getElementById("search");

// Fonction utile pour vider le formulaire, y compris les champs hidden, etc.
const emptyGroupForm = () =>
{
    empyForm(formGroup);
    // Case de suppression et bouton visualisation inutiles en mode création :
    deleteCheckBox.style.display="none";
    btnPreviewGroup.style.display="none";   
    // Intro à vider !
    divGroupIntro.innerHTML="";
}

// Fonction affichant les infos connues concernant un groupe.
const showFormGroupInfos = (id, token) =>
{
    // on commence par tout vider, des fois que... :
    emptyGroupForm();
    const xhrGetInfos = new XMLHttpRequest();
    xhrGetInfos.open("GET", apiUrl+configQuestionnaires.groupRoutes+configQuestionnaires.getGroupRoute+id);
    xhrGetInfos.onreadystatechange = function()
    {
        if (this.readyState == XMLHttpRequest.DONE)
        {
            let response=JSON.parse(this.responseText);
            if (this.status === 200 && response.Group != undefined)
            {
                const mapText =
                {
                    GROUP_ID : response.Group.id,
                    DATE_CREA : dateFormat(response.Group.createdAt),
                    DATE_UPDATE : dateFormat(response.Group.updatedAt),
                    NB_ELEMENTS : (response.Questionnaires!==undefined) ? response.Questionnaires.length : 0
                };
                const groupIntro=replaceAll(infosGroupForAdmin, mapText);
                addElement(divGroupIntro, "p", groupIntro, "", ["info"]);
                for(let data in response.Group)
                {
                    if(formGroup.elements[data]!==undefined)
                    {
                        if(data==="publishingAt" && response.Group[data]!==null)
                            formGroup.elements[data].value=dateFormat(response.Group[data], "form");// !! format pouvant poser soucis si navigateur ne gère pas les champs de type "date"
                        else
                           formGroup.elements[data].value=response.Group[data];
                    }
                }
                deleteCheckBox.style.display="block";
                btnPreviewGroup.style.display="block";
                if(response.Group["isPublishable"] === false)
                    btnPreviewGroup.setAttribute("href", apiUrl+configQuestionnaires.groupRoutes+configQuestionnaires.previewGroupRoutes+"/"+id+"/"+token);
                else
                    btnPreviewGroup.setAttribute("href", siteUrl+"/"+configQuestionnaires.dirWebGroups+"/"+response.Group["slug"]+".html");
                // affichage des éléments du groupe :
                if(response.Questionnaires !== undefined && response.Questionnaires.length !== 0)
                {
                    let listHTML="", optionsDayStr = { weekday: 'long'};
                    for(let i in response.Questionnaires)
                        listHTML+="<li><a href='"+configTemplate.questionnairesManagementPage+"?id="+response.Questionnaires[i].Questionnaire.id+"' id='questionnaire_"+response.Questionnaires[i].Questionnaire.id+"'>"+response.Questionnaires[i].Questionnaire.title+"</a></li>";
                    if(response.Questionnaires.length!==0)
                        addElement(divQuestionnaires, "h3", groupQuestionnairesList.replace("#NB", response.Questionnaires.length));
                    else
                        addElement(divQuestionnaires, "h3", groupQuestionnairesListWithout);                    
                    addElement(divQuestionnaires, "ul", listHTML, "", "", "", false);
                }
            }
        }
    }
    xhrGetInfos.setRequestHeader("Authorization", "Bearer "+token);
    xhrGetInfos.send();
}

const initialise = async () =>
{
    try
    {
        const isConnected=await checkSession(["manager", "admin"], "/"+configTemplate.connectionPage, { message: needBeConnected, color:"error" }, window.location);
        if(isConnected)
        {
            const user=getLocaly("user", true);
            divMain.style.display="block";
            if(!isEmpty(getLocaly("message")))
            {
                addElement(divMessage, "p", getLocaly("message", true).message, "", [getLocaly("message", true).color], "", false);
                removeLocaly("message");
            }
            // Initialisation des formulaires :
            setAttributesToInputs(configQuestionnaires, formSearch);
            setAttributesToInputs(configQuestionnaires.Group, formGroup);
            emptyGroupForm();
            // Si un id est passé par l'url, on essaye d'afficher les infos :
            let urlDatas=getUrlParams();
            if(urlDatas && urlDatas.id!==undefined)
                showFormGroupInfos(urlDatas.id, user.token);
            // Besoin d'un coup de Kärcher ? 
            btnNewGroup.addEventListener("click", function(e)
            {
                emptyGroupForm();
            });
            // Envoi du formulaire principal :
            formGroup.addEventListener("submit", function(e)
            {
                e.preventDefault();
                divResponse.innerHTML="";
                let datas=getDatasFromInputs(formGroup);
                const xhrGroupDatas = new XMLHttpRequest();
                if(!isEmpty(datas.id) && (datas.deleteOk !== undefined))
                    xhrGroupDatas.open("DELETE", apiUrl+configQuestionnaires.groupRoutes+"/"+datas.id);
                else if(!isEmpty(datas.id))
                    xhrGroupDatas.open("PUT", apiUrl+configQuestionnaires.groupRoutes+"/"+datas.id);// mise à jour
                else
                    xhrGroupDatas.open("POST", apiUrl+configQuestionnaires.groupRoutes);// nouvel enregistrement
                xhrGroupDatas.onreadystatechange = function()
                {
                    if (this.readyState == XMLHttpRequest.DONE)
                    {
                        let response=JSON.parse(this.responseText);
                        if (this.status === 201 && response.id != undefined) // nouvel enregistrement créé
                        {
                            addElement(divResponse, "p", addOkMessage, "", ["success"]);
                            datas.id=response.id; // utile pour réaffichage + bas
                        }
                        else if (this.status === 200 && response.message != undefined) // mise à jour ou suppression ok
                        {
                            if(Array.isArray(response.message))
                                response.message = response.message.join("<br>");
                            else
                                response.message = response.message;
                            addElement(divResponse, "p", response.message, "", ["success"]);
                        }
                        else if (response.errors)
                        {
                            if(Array.isArray(response.errors))
                                response.errors = response.errors.join("<br>");
                            else
                                response.errors = serverError;
                            addElement(divResponse, "p", response.errors, "", ["error"]);
                        }
                        else
                            addElement(divResponse, "p", serverError, "", ["error"]);
                        if(isEmpty(response.errors))
                        {
                            if(datas.deleteOk === undefined)
                                showFormGroupInfos(datas.id, user.token);// actualisation de l'affichage après traitement serveur
                            else
                                emptyGroupForm();
                        }
                    }
                }
                xhrGroupDatas.setRequestHeader("Content-Type", "application/json");
                xhrGroupDatas.setRequestHeader("Authorization", "Bearer "+user.token);
                if(datas)
                    xhrGroupDatas.send(JSON.stringify(datas));
            });

            // Envoi d'une recherche
            formSearch.addEventListener("submit", function(e)
            {
                e.preventDefault();
                let datas=getDatasFromInputs(formSearch);
                const xhrSearch = new XMLHttpRequest();
                xhrSearch.open("POST", apiUrl+configQuestionnaires.groupRoutes+configQuestionnaires.searchGroupsRoute);
                xhrSearch.onreadystatechange = function()
                {
                    if (this.readyState == XMLHttpRequest.DONE)
                    {
                        let response=JSON.parse(this.responseText);
                        if (this.status === 200 && Array.isArray(response))
                        {
                            if(response.length === 0)
                                addElement(divSearchResult, "p", searchWithoutResult, "", ["info"]);
                            else
                            {
                                let selectHTML="<option value=''></option>";
                                for(let i in response)
                                    selectHTML+="<option value='"+response[i].id+"'>"+response[i].title+"</option>";
                                addElement(divSearchResult, "select", selectHTML, "selectSearch");
                                const searchSelect=document.getElementById("selectSearch");
                                searchSelect.addEventListener("change", function()
                                {
                                    if(searchSelect.value !== "")
                                        showFormGroupInfos(searchSelect.value, user.token);
                                });
                            }
                        }
                        else if (response.errors)
                        {
                            if(Array.isArray(response.errors))
                                response.errors = response.errors.join("<br>");
                            else
                                response.errors = serverError;
                            addElement(divSearchResult, "p", response.errors, "", ["error"]);
                        }
                        else
                            addElement(divSearchResult, "p", serverError, "", ["error"]);
                    }
                }
                xhrSearch.setRequestHeader("Content-Type", "application/json");
                xhrSearch.setRequestHeader("Authorization", "Bearer "+user.token);
                if(datas)
                    xhrSearch.send(JSON.stringify(datas));
            });
        }
    }
    catch(e)
    {
        addElement(divCrash, "p", serverError, "", ["error"]);
        console.error(e);
    }
}
initialise();
helloDev();