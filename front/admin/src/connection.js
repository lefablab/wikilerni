// -- GESTION DU FORMULAIRE PERMETTANT DE SE CONNECTER

/// L'utilisateur peut avoir répondu à un quiz avant d'arriver sur la page de connexion.
/// Dans ce cas il faut enregistrer son résultat en même temps, une fois la connexion validée.

/// Le connexion peut se faire directement ici via la saisie d'un mot de passe ou via l'envoi d'un token par e-mail.

// Fichier de configuration tirés du backend :
import { apiUrl, availableLangs, siteUrl, theme } from "../../../config/instance.js";
const lang=availableLangs[0];

import { connectionRoute, getLoginLinkRoute, userRoutes } from "../../../config/users.js";
const configTemplate = require("../../../views/"+theme+"/config/"+lang+".js");

// Importation des fonctions utiles au script :
import { getLocaly, removeLocaly, saveLocaly } from "./tools/clientstorage.js";
import { addElement } from "./tools/dom.js";
import { helloDev } from "./tools/everywhere.js";
import { getDatasFromInputs } from "./tools/forms.js";
import { isEmpty } from "../../../tools/main";
import { checkAnswerDatas, checkSession, getTimeDifference, setSession } from "./tools/users.js";

// Dictionnaires :
const { serverError } = require("../../../lang/"+lang+"/general");
const { alreadyConnected, needChooseLoginWay } = require("../../../lang/"+lang+"/user");

// Principaux éléments du DOM manipulés :
const myForm = document.getElementById("connection");
const divMessage = document.getElementById("message");
const divResponse = document.getElementById("response");

helloDev();

// Test de connexion de l'utilisateur + affichage formulaire d'inscription.
const initialise = async () =>
{
    try
    {
        const isConnected=await checkSession();
        if(isConnected)
        {
            saveLocaly("message", { message: alreadyConnected, color:"info" });// pour l'afficher sur la page suivante
            const user=getLocaly("user", true);
            const homePage=user.status+"HomePage";
            console.log("./"+configTemplate[homePage]);
            window.location.assign("./"+configTemplate[homePage]);
        }
        else
        {
            myForm.style.display="block";
            if(!isEmpty(getLocaly("message")))
            {
                addElement(divMessage, "p", getLocaly("message", true).message, "", [getLocaly("message", true).color]);
                removeLocaly("message");
            }
        }     
    }
    catch(e)
    {
        addElement(divResponse, "p", serverError, "", ["error"]);
        console.error(e);
    }
}
initialise();

// Traitement de l'envoi des données de connexion :
myForm.addEventListener("submit", function(e)
{
    try
    {
        e.preventDefault();
        divResponse.innerHTML="";// efface d'éventuels messages déjà affichés
        let datas=getDatasFromInputs(myForm);
        if(isEmpty(datas.password) && isEmpty(datas.getLoginLink))
            addElement(divResponse, "div", needChooseLoginWay, "", ["error"]);
        else
        {
            const xhr = new XMLHttpRequest();
            if(!isEmpty(datas.getLoginLink))
                xhr.open("POST", apiUrl+userRoutes+getLoginLinkRoute);
            else
                xhr.open("POST", apiUrl+userRoutes+connectionRoute);
            xhr.onreadystatechange = function()
            {
                if (this.readyState == XMLHttpRequest.DONE)
                {
                    let response=JSON.parse(this.responseText);
                    if (this.status === 200)
                    {
                        if(!isEmpty(response.message)) 
                        {   // cas d'une demande de lien de connexion avec succès.
                            myForm.style.display="none";
                            addElement(divResponse, "p", response.message, "", ["success"]);
                        }
                        else if(!isEmpty(response.userId) && !isEmpty(response.connexionTime) && !isEmpty(response.token))
                        {   // cas d'une connexion via mot de passe avec succès : on crée une session de connexion et redirige l'utilisateur.
                            let connexionMaxTime=Date.now();
                            if(response.connexionTime.endsWith("days"))// l'utilisateur a demandé à rester connecté sur la durée.
                                connexionMaxTime+=parseInt(response.connexionTime,10)*24*3600*1000;
                            else
                                connexionMaxTime+=parseInt(response.connexionTime,10)*3600*1000;
                            setSession(response.userId, response.token, connexionMaxTime);
                            removeLocaly("lastAnswer");// ! important pour ne pas enregister plusieurs fois son éventuel résultat au quiz.                        
                            myForm.style.display="none";
                            // l'utilisateur peut avoir tenté d'accéder à une autre page que sa page d'accueil :
                            let url=getLocaly("url", true);
                            if(!isEmpty(url) && url.href.indexOf(siteUrl)!==-1)
                            {
                                url=url.href;
                                removeLocaly("url");
                            }
                            else
                                url=configTemplate[response.status+"HomePage"];
                            window.location.assign(url);
                        }
                        else
                            addElement(divResponse, "p", serverError, "", ["error"]);
                    }
                    else if (response.errors)
                    {
                        if(Array.isArray(response.errors))
                            response.errors = response.errors.join("<br>");
                        else
                            response.errors = serverError;
                        addElement(divResponse, "p", response.errors, "", ["error"]);
                    }
                    else
                        addElement(divResponse, "p", serverError, "", ["error"]);
                }
            }
            xhr.setRequestHeader("Content-Type", "application/json");
            if(datas)
            {
                datas.timeDifference=getTimeDifference();
                // Si l'utilisateur a répondu à un quiz, j'ajoute les infos de son résultat aux données envoyées :
                datas=checkAnswerDatas(datas);
                xhr.send(JSON.stringify(datas));
            }
        }
    }
    catch(e)
    {
        addElement(divResponse, "p", serverError, "", ["error"]);
        console.error(e);
    }
});