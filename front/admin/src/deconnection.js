// -- GESTION DE LA PAGE DE DÉCONNEXION

/// On se contente ici de supprimer la session stockée côté client

// Fichier de configuration côté client :
import { apiUrl, availableLangs, theme } from "../../../config/instance.js";
const lang=availableLangs[0];

// Importation des fonctions utile au script :
import { removeLocaly } from "./tools/clientstorage.js";
import { addElement } from "./tools/dom.js";
import { helloDev } from "./tools/everywhere.js";

// Dictionnaires :
const { serverError } = require("../../../lang/"+lang+"/general");
const { byebyeMessage } = require("../../../lang/"+lang+"/user");

const divResponse = document.getElementById("response");

helloDev();

const initialise = async () =>
{
    try
    {
        removeLocaly("user");
        removeLocaly("url");
        removeLocaly("message");
        addElement(divResponse, "p", byebyeMessage, "", ["success"]);        
    }
    catch(e)
    {
        addElement(divResponse, "p", serverError, "", ["error"]);
    }
}
initialise();