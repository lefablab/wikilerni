// -- PAGE D'ACCUEIL POUR LES GESTIONNAIRES

/// Vérifie que l'utilisateur est bien connecté, a le bon statut et le redirige vers le formulaire d'inscription si ce n'est pas le cas.
/// Si c'est ok, on récupère les stats générales et des dernières 24H et les affiche
/// On liste ensuite les prochains quizs devant être publiés en faisant ressortir ceux devant être complétés + la prochaine date sans quiz
/// Un menu permet à l'utilisateur d'accéder aux formulaires permettant de gérer les quizs et les comptes utilisateurs et abonnements
/// Un message venant d'une autre page peut aussi être à afficher lors du premier chargement.

/// Temporairement, c'est ici aussi que l'on peut régénérer tout le HTML -> à terme dans homeAdmin !

// Fichier de configuration côté client :
import { apiUrl, availableLangs, theme } from "../../../config/instance.js";
const lang=availableLangs[0];

import { getAdminStats, userRoutes } from "../../../config/users.js";
import { getListNextQuestionnaires, questionnaireRoutes, regenerateHTML } from "../../../config/questionnaires.js";
const configTemplate = require("../../../views/"+theme+"/config/"+lang+".js");

// Fonctions utiles au script :
import { getLocaly, removeLocaly } from "./tools/clientstorage.js";
import { addElement } from "./tools/dom.js";
import { helloDev } from "./tools/everywhere.js";
import { dateFormat, isEmpty, replaceAll } from "../../../tools/main";
import { checkSession } from "./tools/users.js";

// Dictionnaires :
const { notAllowed, serverError, statsAdmin } = require("../../../lang/"+lang+"/general");
const { nextDateWithoutQuestionnaire, nextQuestionnairesList, questionnaireNeedBeCompleted } = require("../../../lang/"+lang+"/questionnaire");
const { welcomeMessage } = require("../../../lang/"+lang+"/user");

// Principaux éléments du DOM manipulés :
const divMain = document.getElementById("main-content");
const divCrash = document.getElementById("crash");
const divMessage = document.getElementById("message");
const divQuestionnaires = document.getElementById("questionnaires");
const btnRegenerate = document.getElementById("wantRegenerate");

helloDev();

const initialise = async () =>
{
    try
    {        
        const isConnected=await checkSession(["manager", "admin"], "/"+configTemplate.connectionPage, { message: notAllowed, color:"error" });
        if(isConnected)
        {
            const user=getLocaly("user", true);
            addElement(divMessage, "h2", welcomeMessage.replace("#NAME", user.name));
            divMain.style.display="block";
            if(!isEmpty(getLocaly("message")))
            {
                addElement(divMessage, "p", getLocaly("message", true).message, "", [getLocaly("message", true).color], "", false);
                removeLocaly("message");
            }
            // Les stats sur les comptes utilisateurs :
            const xhrStats = new XMLHttpRequest();
            xhrStats.open("GET", apiUrl+userRoutes+getAdminStats);
            xhrStats.onreadystatechange = function()
            {
                if (this.readyState == XMLHttpRequest.DONE)
                {
                    let response=JSON.parse(this.responseText);
                    if (this.status === 200)
                    {
                        const mapText =
                        {
                            NB_USERS_24H : response.nbNewUsers24H,
                            NB_SUBSCRIPTIONS_24H : response.Subscriptions.nbSubscriptions24H,
                            NB_USERS_DELETED_24H : response.nbDeletedUsers24H,
                            NB_ANSWERS_24H : response.Answers.nbAnswers24H,
                            NB_USERS_TOT : response.nbNewUsersTot,
                            NB_SUBSCRIPTIONS_TOT : response.Subscriptions.nbSubscriptionsTot,
                            NB_SUBSCRIPTIONS_PREMIUM : response.Subscriptions.nbSubscriptionsPremium,
                            NB_ANSWERS_TOT : response.Answers.nbAnswersTot,
                            NB_USERS_DELETED_TOT : response.nbDeletedUsersTot,
                            NB_USERS_DELETED_VALIDED : response.nbDeletedUsersWasValided,
                            NB_USERS_DELETED_PREMIUM : response.nbDeletedUsersTotWasPremium
                        };
                        addElement(divMessage, "p", replaceAll(statsAdmin, mapText), "", "", "", false);
                    }
                }
            }
            xhrStats.setRequestHeader("Authorization", "Bearer "+user.token); 
            xhrStats.send();
            
            // Les questionnaires bientôt publiés :      
            const xhrNextQuestionnaires = new XMLHttpRequest();
            xhrNextQuestionnaires.open("GET", apiUrl+questionnaireRoutes+getListNextQuestionnaires);
            xhrNextQuestionnaires.onreadystatechange = function()
            {
                if (this.readyState == XMLHttpRequest.DONE)
                {
                    let response=JSON.parse(this.responseText);
                    if (this.status === 200 && Array.isArray(response.questionnaires))
                    {
                        let listHTML="", dayStr, optionsDayStr = { weekday: 'long'};
                        for(let i in response.questionnaires)
                        {
                            dayStr=new Intl.DateTimeFormat(lang, optionsDayStr).format(new Date(response.questionnaires[i].datePublishing));
                            listHTML+="<li>"+dayStr+" "+dateFormat(response.questionnaires[i].datePublishing, availableLangs[0])+": <a href='"+configTemplate.questionnairesManagementPage+"?id="+response.questionnaires[i].id+"'>"+response.questionnaires[i].title+"</a>";
                            if(response.questionnaires[i].isPublishable===false)
                                listHTML+=" <span class='error'>("+questionnaireNeedBeCompleted+")</li>";
                            listHTML+="</li>";
                        }
                        if(response.questionnaires.length!==0)
                            addElement(divQuestionnaires, "h2", nextQuestionnairesList.replace("#NB", response.questionnaires.length));
                        addElement(divQuestionnaires, "h4", nextDateWithoutQuestionnaire+dateFormat(response.dateNeeded, availableLangs[0]), "", ["info"], "", false);
                        addElement(divQuestionnaires, "ul", listHTML, "", "", "", false);
                    }
                }
            }
            xhrNextQuestionnaires.setRequestHeader("Authorization", "Bearer "+user.token); 
            xhrNextQuestionnaires.send();

            // Traitement demande régénérer HTML
            btnRegenerate.addEventListener("click", function(e)
            {
                e.preventDefault();
                const xhrRegenerate = new XMLHttpRequest();
                xhrRegenerate.open("GET", apiUrl+questionnaireRoutes+regenerateHTML);
                xhrRegenerate.onreadystatechange = function()
                {
                    if (this.readyState == XMLHttpRequest.DONE)
                    {
                        let response=JSON.parse(this.responseText);
                        if (this.status === 200 && response.message!=undefined)
                            addElement(divMessage, "p", response.message, "", ["success"], "", false);
                        else
                            addElement(divMessage, "p", serverError, "", ["error"], "", false);
                    }
                }
                xhrRegenerate.setRequestHeader("Content-Type", "application/json");
                xhrRegenerate.setRequestHeader("Authorization", "Bearer "+user.token);
                xhrRegenerate.send();
            });
        }
        
    }
    catch(e)
    {
        addElement(divCrash, "p", serverError, "", ["error"]);
        console.error(e);
    }
}
initialise(); 