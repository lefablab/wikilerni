const path = require('path');

module.exports =
{
  mode: "development",
  entry:
  {
    connection: "./src/connection.js",
    deconnection: "./src/deconnection.js",
    homeManager: "./src/homeManager.js",
    manageGroups: "./src/manageGroups.js",
    manageQuestionnaires: "./src/manageQuestionnaires.js",
    manageUsers: "./src/manageUsers.js",
    polyfill: "babel-polyfill"
  },
  output:
  {
    path: path.resolve(__dirname, "www"),
    filename: "JS/[name].app.js",
  },
  module:
  {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  devServer:
  {
    static: path.resolve(__dirname, "./www")
  },
};