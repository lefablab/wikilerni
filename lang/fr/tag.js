module.exports =
{
    needName: "Merci de donner un nom à cette nouvelle étiquette.",
    needNotTooLongName: "Merci de saisir un nom d'étiquette ne contenant pas plus de 100 caractères.",
    needUniqueName: "Une étiquette ayant le même nom existe déjà.",
    needUniqueUrl : "Une étiquette ayant la même url existe déjà.",
    needUrl : "Merci de saisir une url pour cette étiquette.",
    notFound: "Le tag n'a pas été trouvé.",
    tagMetaDescription: "Une sélection d'articles et de quizs sur le thème : ",
    tagsForQuestionnaireNotFound: "Il manque la liste des tags actuellement associés à ce questionnaire."
};