module.exports =
{
    btnSendResponse: "Testez vos réponses.",
    btnShareQuizTxt: "Partager ce quiz sur ",   
    commonIntroTxt: "Ce quiz vous permet tester ce que vous avez retenu des articles qui vous ont été proposés précédemment à la lecture. Au besoin, cliquez sur le bouton ci-dessous pour les (re)lire.",
    correctAnswerTxt: "Bonne réponse",    
    groupsName: "Quiz",// nom d'un groupe pour l'affichage dans les vues
    groupQuestionnairesList : "Les #NB éléments enregistrés pour ce groupe",
    groupQuestionnairesListWithout : "Aucun élément n'a été enregistré pour ce groupe.",  
    haveBeenPublished: "#NB nouveaux groupes de quizs ont été publiés.",
    infosGroupForAdmin: "Ce groupe de quizs a été créé le DATE_CREA, mise à jour la dernière fois le DATE_UPDATE.<br>Son identifiant est <b>GROUP_ID</b>. Il regroupe actuellement les questions de NB_ELEMENTS quizs.",
    linkFirstElementGroup: "Retour au premier article.",
    lastUpdated: "Dernière mise à jour le ",    
    needCorrectPublishingDate: "La date de publication fournie n'a pas un format valide.",
    needLanguage: "Vous devez sélectionner la langue de ce groupe de quizs.",
    needNotTooLongTitle: "Le titre du groupe de quizs ne doit pas compter plus de 255 caractères.",                 
    needTitle: "Merci de fournir un titre à ce groupe de quizs.",
    needUniqueUrl: "L'url du groupe de quizs doit être unique.",
    needUrl: "Merci de fournir l'url à ce groupe de quizs.",
    notFound: "Le groupe de quizs (#SEARCH) n'a pas été trouvé.",
    publishedAt: " le",
    publishedBy: "Quiz publié par",
    searchIsNotLongEnough: "Vous devez saisir au moins #MIN caractères pour votre recherche.",
    searchWithoutResult: "Aucun groupe n'a été trouvé pour votre recherche.",
    wrongAnswerTxt: "Mauvaise réponse"
};