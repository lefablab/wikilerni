module.exports =
{
    addOkMessage: "La question a bien été ajoutée.",
    addQuestionTxt: "Ajouter une question",
    deleteOkMessage: "La question a bien été supprimée.",
    introNoQuestion: "Aucune question n'a été saisie pour l'instant",
    introTitleForQuestion: "Les questions enregistrées",
    needMaxQuestions: "Vous avez déjà atteint le nombre maximal de questions pour ce questionnaire : ",         
    needNotTooLongText: "La question ne doit pas compter plus de 255 caractères.",
    needNumberForRank : "Vous devez fournir un nombre supérieur ou égal à 1 pour le rang de cette question.", 
    needQuestionnaire: "Le questionnaire concerné n'a pas été trouvé.",
    needText: "Merci de saisir le texte de la question !",   
    notFound: "L'enregistrement de la question n'a pas été trouvé.",
    updateOkMessage: "La question a bien été modifiée."
};