module.exports =
{
    lastQuestionnairesForUser: "Vos dernières lectures :",
    notFound : "Les informations d'un questionnaire attribué à un abonné n'ont pas été trouvées : ",
    noQuestionnaireAccess: "Aucun article ne vous a encore été envoyé par e-mail. Sans doute, venez-vous de créer votre compte. En attendant d'en recevoir, vous pouvez mieux découvrir le site WikiLerni <a href=\"/a-propos.html\">en lisant la page à propos</a> ou encore commencer à parcourir librement son contenu à l'aide du moteur de recherche ci-dessus.",
    questionnaireRetryInfo : "Vous avez déjà reçu tous les articles et quizs publiés à ce jour. En attendant la publication de nouveaux contenus, vous pouvez peut-être réessayer de répondre au quiz suivant ?",
    questionnaireRetryInfoTxt : "Bonjour USER_NAME,\n\nVous avez déjà reçu tous les quizs publiés à ce jour ! En attendant la publication de nouveaux quizs, vous pouvez peut-être réessayer le suivant ?\n\nQUESTIONNAIRE_URL\n\nBonne lecture !\n\nStopper les envois ?\nUNSUBSCRIBE_URL",
    searchIsNotLongEnough : "Merci de fournir un mot-clés d'au moins deux caractères pour votre recherche."
};