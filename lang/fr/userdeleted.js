module.exports =
{
    needValidCreationDate: "La date de création du compte fournie n'a pas un format valide.",
    needValidDeleteDate: "La date de suppression du compte fournie n'a pas un format valide.",
    needKnowIfWasPremium: "Nous devons savoir si ce compte avait un abonnement prémium ou pas.",
    needKnowIfWasValided: "Nous devons savoir si ce compte était validé ou pas."
};