module.exports =
{
    btnProposeConnection: "Je me connecte.",// déplacé dans general.js
    btnProposeSubscribe: "Je crée mon compte.",//idem
    btnProposeSave: "Enregistrer mes réponses.",//idem
    btnSendResponse: "Testez vos réponses.",
    btnShareQuizTxt: "Partager via ",
    btnShareQuizMailBody: "Bonjour,%0A%0AVoici%20un%20lien%20internet%20qui%20devrait%20t'intéresser :%0A",    
    btnShowQuestionnaire: "Répondre au quiz !",
    correctAnswerTxt: "Bonne réponse",
    estimatedTime: "Durée de lecture estimée : ",
    estimatedTimeOption :
    {
        short: "courte",
        medium: "moyenne",
        long: "longue"
    },
    explanationBeforeTxt: "Extrait :",
    haveBeenPublished : ":NB nouveaux questionnaires ont été publiés.",
    haveBeenRegenerated : "Les pages HTML de #NB1 questionnaires ou éléments de groupes, #NB2 quizs groupés et #NB3 thèmes ont été regénérés.",
    lastUpdated: "Dernière mise à jour, le ",
    linkGoToNextElement: "Article suivant",
    linkGoToQuiz: "Accéder au quiz",    
    needCorrectPublishingDate: "La date de publication fournie n'a pas un format valide.",
    needEstimatedTime: "Merci de sélectionner une estimation de la durée de ce quiz.",
    needGroupIfRank: "Vous avez saisi un rang de classement, sans sélectionner le groupe du quiz.",
    needIntroduction: "Merci de fournir un texte d'introduction à votre quiz.",
    needKnowIfIsPublished: "Il faut savoir si ce quiz est publié.",
    needLanguage: "Vous devez sélectionner la langue de ce quiz.",
    needNotTooLongTitle: "Le titre du quiz ne doit pas compter plus de 255 caractères.",
    needNumberForRank: "Vous devez saisir un nombre entier pour le rang de ce questionnaire dans son groupe.",              
    needTitle: "Merci de fournir un titre à votre quiz.",
    needUniqueUrl: "L'url du quiz doit être unique.",
    needUrl: "Merci de fournir l'url à votre quiz.",
    nextDateWithoutQuestionnaire: "Prochaine date sans quiz programmé : ",
    nextQuestionnairesList: "Les #NB prochains quizs devant être publiés",
    questionnairesName: "quiz",
    questionnaireNeedBeCompleted: "Quiz incomplet",
    publishedAt: ", le",
    publishedBy: "Quiz publié par",
    searchQuestionnaireResultTitle : "Résultat pour votre recherche",
    searchQuestionnaireWithNoResult : "Aucun quiz n'a été trouvé pour votre recherche.",
    searchQuestionnaireWithResult : "Il y a #NB article#S correspondant à votre recherche :",
    wrongAnswerTxt: "Mauvaise réponse"
};