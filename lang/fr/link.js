module.exports =
{
    addedOkMessage: "Le lien a bien été ajouté.",
    addLinkTxt: "Ajouter un lien",
    defaultValueForLink: "Lire l'article sur Wikipédia.",
    deletedOkMessage: "Le lien a bien été supprimé.",
    introNoLink : "Aucun lien pour l'instant.",
    introTitleForLink : "Lectures proposées",
    needAnchor : "Merci de fournir une ancre pour ce lien.",
    needGoodLongAnchor : "Merci de fournir une ancre pour ce lien comptant entre 5 et 150 caractères.",
    needMaxLinksForQuestionnaire : "Vous avez déjà atteint le nombre maximal de liens pour ce questionnaire.",
    needNotTooLongUrl : "Merci de  saisir un url ne comptant pas plus de 255 caractères.",
    needQuestionnaire : "Le questionnaire du lien n'a pas été trouvé.",
    needUrl : "Merci de  saisir l'url du lien.",
    needValidUrl : "Merci de  saisir un url ayant un format valide.",
    notFound : "L'enregistrement du lien n'a pas été trouvé.",
    updatedOkMessage: "Le lien a bien été modifié."
};