module.exports =
{
    createdOkMessage: "La nouvelle période de pause a bien été enregistrée.",
    deletedOkMessage: "La période de pause a bien été supprimée.",
    needEndingDate : "Merci de fournir la date de fin de la période de pause.",
    needGoodLongName : "Le nom saisi pour cette période de pause ne doit pas compter plus de 255 caractères.",
    needMinEndingDate : "La date de fin doit être supérieure à celle de début.",
    needName : "Merci de saisir un nom pour cette période de pause.",
    needStartingDate : "Merci de fournir la date de début de la période de pause.",
    needValidEndingDate : "La date de fin fournie n'a pas un format valide.",
    needValidStartingDate : "La date de début fournie n'a pas un format valide.",
    updatedOkMessage: "La période de pause a bien été modifiée."
};