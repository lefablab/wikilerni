module.exports =
{
    localDBGetPreviousResultsFail: "Bug de récupération des résultats précédents.",
    localDBNeedDatas: "Il manque des données nécessaires à l'enregistrement.",
    localDBNeedQuizId: "Aucun identifiant n'a été fourni pour le quiz.",
    localDBNotReady: "Désolé, mais il semble que votre navigateur ne permette pas l'utilisation de cette page. Pour plus d'informations, lisez les explications ci-dessous.",
    localFileFail: "Le fichier que vous avez sélectionné n'a pas le format attendu (JSON).",
    localFileImportOK: "Vos données ont bien été importées. Vos quizs devraient apparaître ci-dessus.",
    needIntegerNumberCorrectResponses : "Le nombre de réponses correctes doit être un nombre entier.",
    needIntegerNumberSecondesResponse : "La durée de la réponse doit être un nombre entier de secondes.",
    needIntegerNumberUserResponses : "Le nombre de questions auxquelles l'utilisateur a répondu doit être un nombre entier.",
    needMaxNumberUserResponses : "Le nombre de questions auxquelles l'utilisateur a répondu ne peut être aussi élevé.",
    needMinNumberUserResponses : "Le nombre de questions auxquelles l'utilisateur a répondu ne peut être inférieur à 1.",
    needNumberCorrectResponses : "Le nombre de réponses correctes doit être fourni.",
    needNumberUserResponses : "Le nombre de questions auxquelles l'utilisateur a répondu doit être fourni.",
    needMaxNumberCorrectResponses : "Le nombre de réponses correctes ne peut être supérieur au nombre de questions.",
    needMinNumberCorrectResponses : "Le nombre de réponses correctes ne peut être négatif.",
    needMinNumberSecondesResponse : "La durée de la réponse ne peut être négative.",
    noPreviousResults: "On dirait que c'est la première fois que vous répondez à ce quiz. Bonne lecture !",
    noPreviousResultsAtAll: "Vous n'avez répondu à aucun quiz pour l'instant !",
    previousResultsLine: "Le DATEANSWER, vous avez répondu correctement à NBCORRECTANSWERS questions sur NBQUESTIONS en AVGDURATION secondes.",
    previousResultsStats: "En moyenne, vous avez répondu à ce quiz en AVGDURATION secondes, en ayant <b>AVGCORRECTANSWERS % de bonnes réponses</b>.",
    previousResultsTitle: "Voici vos précédents résultats à ce quiz",
    responseSavedError : "Cependant une erreur a été rencontrée durant l'enregistrement de votre résultat. <a href='/#URL'>Accèder à tous vos quizs</a>.",
    responseSavedMessage : "Votre résultat a été enregistré. <a href='/#URL'>Accèder à tous vos quizs</a>.",
    statsUser: "Vous avez enregistré <b>NBANSWERS réponseS1 à NBQUESTIONNAIRES quizS2 différentS3</b> sur les NBTOTQUESTIONNAIRES proposéS4 par le site.<br>En moyenne, vous avez mis AVGDURATION secondes à répondre et avez <b>correctement répondu à AVGCORRECTANSWERS % des questions</b>.",
    userAnswersFail : "Vous avez répondu en DURATION secondes et avez <u><b>NBCORRECTANSWERS bonne(s) réponse(s) sur NBQUESTIONS questions</b></u>. C'est certain, vous ferez mieux la prochaine fois !",
    userAnswersMedium : "Vous avez répondu en DURATION secondes et avez <u><b>NBCORRECTANSWERS bonne(s) réponse(s) sur NBQUESTIONS questions</b></u>. C'est pas mal du tout !",
    userAnswersSuccess : "Vous avez répondu en DURATION secondes et avez <u><b>NBCORRECTANSWERS bonne(s) réponse(s) sur NBQUESTIONS questions</b></u>. Bravo ! Rien ne vous échappe !",
    wantToSaveResultsDatas: "Sauvegarder les données.",
    wantToSaveResponses: "Si vous le souhaitez, vous pouvez <b>sauvegarder vos résultats</b> (il n'est pas nécessaire de créer un compte) :",
    wantToSeaPreviousResults: "<a href='URL'>Cliquez ici</a> pour tous vos résultats et vos statistiques de réussite à ce quiz."
};