module.exports =
{
    needGoodLongTextForChoice : "Merci de saisir des réponses ne comptant pas plus de 255 caractères.",
    needKnowIsCorrectChoice : "Merci de spécifier si cette réponse est correcte ou pas.",
    needMaxChoicesForQuestion : "Le nombre de réponses possibles pour une question ne peut dépasser : ",
    needMinChoicesForQuestion : "Merci de fournir au moins 2 réponses possibles pour chaque question.",
    needOneGoodChoice : "Merci de désigner au moins une des réponses comme étant correcte.",
    needQuestionForChoices : "Tentative d'ajout/actualisation de réponses possibles pour une question n'ayant pas été trouvée : ",
    needTextForChoice : "Merci de saisir le texte de la réponse proposée.",
};