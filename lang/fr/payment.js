module.exports =
{
    mailPaymentAdminNoticeBodyHTML: "<h3>Bonjour,</h3><p>Un nouvel abonnement de soutien vient d'être enregistré pour l'utilisateur EMAIL.</p>",
    mailPaymentAdminNoticeBodyTxt: "Bonjour,\nUn nouvel abonnement de soutien vient d'être enregistré par l'utilisateur EMAIL.",
    mailPaymentAdminNoticeSubject: "Nouvel abonnement prémium !",
    mailPaymentLinkTxt: "Mon compte WikiLerni.",   
    mailPaymentThankBodyHTML: "<h3>Bonjour USER_NAME,</h3><p>Suite à votre paiement, votre abonnement à SITE_NAME vient d'être prolongé de NBDAYS jours.<br><br>Merci beaucoup et à bientôt sur SITE_NAME !</p>",
    mailPaymentThankBodyTxt: "Bonjour USER_NAME,\n\nSuite à votre paiement, votre abonnement à SITE_NAME vient d'être prolongé de NBDAYS jours.\n\nMerci beaucoup et à bientôt !",
    mailPaymentThankGodfatherBodyHTML: "<h3>Bonjour USER_NAME,</h3><p>Un des utilisateurs que vous avez parrainé vient de souscrire à un abonnement de soutien.<br>En récompense, votre abonnement vient donc d'être prolongé de 30 jours.<br>Merci à vous et à bientôt !</p>",
    mailPaymentThankGodfatherBodyTxt: "Bonjour USER_NAME,\n\nUn des utilisateurs que vous avez parrainé vient de souscrire à un abonnement de soutien.\n\nEn récompense, votre abonnement vient donc d'être prolongé de 30 jours.\n\nMerci à vous et à bientôt !",
    mailPaymentThankGodfatherSubject: "Merci !",
    mailPaymentThankSubject: "Merci !",
    needAmount : "Il manque le montant du paiement.",
    needCode : "Il manque le code de la commande associée à ce paiement.",
    needCorrectAmount : "Le montant du paiement n'a pas un format valide.",
    needName : "Il manque le nom du client ayant payé.",
    paymentDatasFail: "Un retour paiement a été reçu avec des paramètres non conformes : ",
    paymentGodfatherNotFound:  "Un retour paiement a été reçu pour un utilisateur, mais les infos de son parrain n'ont pas été retrouvées : ",
    paymentUrlFail: "Un retour paiement a été reçu avec une url non conforme : ",
    paymentUserNotFound: "Un retour paiement a été reçu avec l'id d'un utilisateur inconnu : "
};