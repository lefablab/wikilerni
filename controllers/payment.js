const config = require("../config/main.js");
const configTpl = require("../views/"+config.theme+"/config/"+config.availableLangs[0]+".js");

const tool = require("../tools/main");
const toolMail = require("../tools/mail");

const txt = require("../lang/"+config.adminLang+"/payment");
const txtUser = require("../lang/"+config.adminLang+"/user");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

const userCtrl = require("./user");

exports.getOneUserPayments = async (req, res, next) =>
{
    try
    {
        const connectedUser=req.connectedUser;  
        if(connectedUser===false || ["admin","manager"].indexOf(connectedUser.User.status) === -1)
            res.status(403).json({ errors: txtGeneral.notAllowed });
        else
        {
            const db = require("../models/index");
            const Payments=await db["Payment"].findAll({ where: { UserId: req.params.id }, order: [["createdAt", "DESC"]] });
            res.status(200).json(Payments);
        }
        next();       
     }
    catch(e)
    { 
        next(e);
    } 
}

exports.saveUserPaymentInfos = async (req, res, next) =>
{
    // exemple d'url Ok : WP-infos.html?dom=wikilerni.com&ref=21&mt=24&cmd=de11de&cl=monsieur+dugenoux&hKey=1b42653d28ecd07b9b3ba202770a1a45
    try
    {
        // l'utilisateur étant incité à venir sur l'API après paiement, dans ce cas on le redirige vers le site
        // ex url retour : /payment/WP-infos.html?article=nomArticle&ht=montantHT&client=nomClient
        if(req.query.article!=undefined)
        {
            res.writeHead(302, { "Location": config.siteUrl+"/merci.html" });
            res.end();
        }
        else
        {           
            require('dotenv').config();
            const ndDaysSubscription=180;// nombre de jour de l'abonnement
            const ndDaysGodFather=30;// nombre de jour en plus si parrainage
            const db = require("../models/index");
            const md5 = require("md5");
            const montantsAbonnement=["9","18","36","54"];
            // !! attention req.query enlève les caractères spéciaux comme les "+" des paramètres de l'url. Il vaut donc mieux utiliser req.url pour comparer avec le hash au reste de la chaîne.
            const testUrl=req.url.slice(req.url.indexOf("?")+1,req.url.lastIndexOf("&"));
            //console.log(testUrl);
            //console.log(md5(testUrl+process.env.MD5_WP));
            if(md5(testUrl+process.env.MD5_WP)!==req.query.hKey) // le hashage est effectué après le remplacement des caractères spéciaux dans l'url.
                throw { message: txt.paymentUrlFail+testUrl };
            else if(req.query.ref==="" || montantsAbonnement.indexOf(req.query.mt) === -1)
                throw { message: txt.paymentDatasFail+testUrl };
            else
            {
                const client=await userCtrl.searchUserById(req.query.ref);
                if(!client)
                    throw { message: txt.paymentUserNotFound+testUrl };
                else
                {
                    // Si cet utilisateur a un parrain on le remercie et lui ajoute 30 jours d'abonnement
                    // Cela impacte aussi la durée à ajouter à l'abonnement du client
                    let numberOfDays=ndDaysSubscription;
                    if(client.User.GodfatherId)
                    {
                        const parrain=await userCtrl.searchUserById(client.User.GodfatherId);
                        if(parrain)
                        {
                            addDays(parrain, ndDaysGodFather);
                            numberOfDays+=ndDaysGodFather;
                            await db["Subscription"].update({ ...parrain.Subscription }, { where: { UserId : client.User.GodfatherId }, fields: ["numberOfDays"], limit:1 });
                            userCtrl.creaUserJson(client.User.GodfatherId);
                            const mapMail =
                            {
                                USER_NAME: parrain.User.name
                            };
                            const mailDatas=
                            {
                                mailSubject: txt.mailPaymentThankGodfatherSubject,
                                mailPreheader: txt.mailPaymentThankGodfatherSubject,
                                mailTitle: txt.mailPaymentThankGodfatherSubject,
                                mailHeaderLinkUrl: config.siteUrl+"/"+configTpl.userHomePage,
                                mailHeaderLinkTxt: txt.mailPaymentLinkTxt,
                                mailMainContent: tool.replaceAll(txt.mailPaymentThankGodfatherBodyHTML, mapMail),
                                linksCTA: [{ url:config.siteUrl+"/"+configTpl.userHomePage, txt:txt.mailPaymentLinkTxt }],
                                mailRecipientAddress: parrain.User.email
                            }
                            await toolMail.sendMail(parrain.User.smtp, parrain.User.email, txt.mailPaymentThankGodfatherSubject, tool.replaceAll(txt.mailPaymentThankGodfatherBodyTxt, mapMail), "", mailDatas);
                        }
                        else
                             res.alerte=txt.paymentGodfatherNotFound+client.User.GodfatherId;
                    }
                    addDays(client, numberOfDays);
                    const infosClient=
                    {
                        clientName: req.query.cl,
                        amount: req.query.mt,
                        codeCommande: req.query.cmd,
                        UserId: client.User.id,
                        numberOfDays: client.Subscription.numberOfDays
                    };
                    await db["Payment"].create({ ...infosClient }, { fields: ["clientName", "amount", "codeCommande", "UserId"] });
                    await db["Subscription"].update({ ...infosClient }, { where: { UserId : infosClient.UserId }, fields: ["numberOfDays"], limit:1 });
                    userCtrl.creaUserJson(infosClient.UserId);
                    const mapMail2 =
                    {
                        SITE_NAME: config.siteName,
                        USER_NAME: client.User.name,
                        NBDAYS: numberOfDays
                    };
                    const mailDatas2 =
                    {
                        mailSubject: txt.mailPaymentThankSubject,
                        mailPreheader: txt.mailPaymentThankSubject,
                        mailTitle: txt.mailPaymentThankSubject,
                        mailHeaderLinkUrl: config.siteUrl+"/"+configTpl.userHomePage,
                        mailHeaderLinkTxt: txt.mailPaymentLinkTxt,
                        mailMainContent: tool.replaceAll(txt.mailPaymentThankBodyHTML, mapMail2),
                        linksCTA: [{ url:config.siteUrl+"/"+configTpl.userHomePage, txt:txt.mailPaymentLinkTxt }],
                        mailRecipientAddress: client.User.email
                    }
                    await toolMail.sendMail(client.User.smtp, client.User.email, txt.mailPaymentThankSubject, tool.replaceAll(txt.mailPaymentThankBodyTxt, mapMail2), "", mailDatas2);
                    // + info admin site
                    await toolMail.sendMail(0, config.adminEmail, txt.mailPaymentAdminNoticeSubject, txt.mailPaymentAdminNoticeBodyTxt.replace("EMAIL", client.User.email), txt.mailPaymentAdminNoticeBodyHTML.replace("EMAIL", client.User.email));
                    res.status(200).json(true);
                }
            }
            next();
        }
    }
    catch(e)
    { 
        next(e);
    }
}

// Ajoute le nombre de jours nécessaire à un abonnement en vérifiant si il est expiré ou non.
// Si l'abonnement est toujours actif, on se contente d'ajouter le nombre de jours souhaité à celui déjà connu
// Sinon, on complète pour arriver à la date actuelle, puis on ajoute le nombre de jours souhaité
const addDays = (user, nbDays) =>
{
    let dateEnd=new Date(user.Subscription.createdAt).getTime()+user.Subscription.numberOfDays*24*3600*1000;
    if(dateEnd < Date.now())
    {
        let needDays=Math.round((Date.now()-dateEnd)/(24*3600*1000));
        nbDays+=needDays;        
    }
    user.Subscription.numberOfDays+=nbDays;
}