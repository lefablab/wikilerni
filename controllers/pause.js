const { QueryTypes  } = require("sequelize");

const config = require("../config/main.js");

const tool = require("../tools/main");
const toolError = require("../tools/error");

const userCtrl=require("./user");

const txt = require("../lang/"+config.adminLang+"/pause");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

exports.create = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");
        const connectedUser=req.connectedUser;
        req.body.SubscriptionId=connectedUser.Subscription.id;
        await db["Pause"].create({ ...req.body });
        userCtrl.creaUserJson(connectedUser.User.id);
        res.status(201).json({ message: txt.createdOkMessage });
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.messages)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}

exports.modify = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");        
        const connectedUser=req.connectedUser;      
        if(!checkPauseIsOk(req.params.id, connectedUser))
            res.status(404).json({ errors: txtGeneral.serverError });
        else
        {
            await db["Pause"].update({ ...req.body }, { where: { id : req.params.id } , fields: ["name", "startingAt", "endingAt"], limit:1 }),
            userCtrl.creaUserJson(connectedUser.User.id);
            res.status(201).json({ message: txt.updatedOkMessage });
        }
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.length!==0)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}

exports.delete = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");    
        const connectedUser=req.connectedUser;      
        if(!checkPauseIsOk(req.params.id, connectedUser))
            res.status(404).json({ errors: txtGeneral.serverError });
        else
        {
            await db["Pause"].destroy({ where: { id : req.params.id }, limit:1 });
            userCtrl.creaUserJson(connectedUser.User.id);
            res.status(200).json({ message: txt.deletedOkMessage });
        }
        next();
    }
    catch(e)
    {
        next(e);
    }
}

// Cron
exports.deleteOldPauses= async(req, res, next) =>
{
    try
    {
        const db = require("../models/index");  
        // on laisse deux jours de rab pour les décalages horaires & co
        const userPauses=await db.sequelize.query("SELECT DISTINCT `Subscriptions`.`UserId` FROM `Subscriptions` INNER JOIN `Pauses` ON `Subscriptions`.`id`=`Pauses`.`SubscriptionId` WHERE ADDDATE(`endingAt`, 2) < NOW()", { type: QueryTypes.SELECT  });
        if(userPauses.length!==0)
        {
            await db.sequelize.query("DELETE FROM `Pauses` WHERE ADDDATE(`endingAt`, 2) < NOW()");
            for(i in userPauses)
                await userCtrl.creaUserJson(userPauses[i].UserId);
        }
        res.status(200).json(true);
        next();
    }
    catch(e)
    {
        next(e);
    }
}


// FONCTIONS UTILITAIRES

// Vérifie si la période de pause appartient bien à cet utilisateur
const checkPauseIsOk = (idPause, user) =>
{
    if(!user.Pauses)
        return false;
    let PauseIsOk=false, i=0;
    while (!PauseIsOk && user.Pauses[i])
    {
        if(user.Pauses[i].id == idPause)// ! n'ont pas forcément le même type
            PauseIsOk=true;
        else
            i++;
    }
    return PauseIsOk;
}