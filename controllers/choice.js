const config = require("../config/main.js");

const tool = require("../tools/main");
const toolError = require("../tools/error");

const questionCtrl = require("./question");
const questionnaireCtrl = require("./questionnaire");

const txt = require("../lang/"+config.adminLang+"/choice");
const txtQuestion = require("../lang/"+config.adminLang+"/question");

// J'arrive aux deux contrôleurs suivants après être les contrôleurs de "question" qui leur passe la main via next()

exports.create = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");
        let question=await questionCtrl.searchQuestionById(req.body.QuestionId);
        if(!question)
            throw { message: txt.needQuestionForChoices+req.body.QuestionId };
        let choices=[], i=0, oneIsCorrect=false;
        while(!tool.isEmpty(req.body["choiceText"+i]))
        {
            if(req.body["choiceIsCorrect"+i]=="true")
            {
                isCorrect=true;
                oneIsCorrect=true;
            }
            else
                isCorrect=false;
            choices.push({ text:req.body["choiceText"+i], isCorrect:isCorrect, QuestionId:req.body.QuestionId });
            i++;
        }
        if(!oneIsCorrect)
        {
            questionCtrl.deleteQuestionById(req.body.QuestionId);
            res.status(400).json({ errors: [txt.needOneGoodChoice] });
        }
        else if(choices.length < 2)
        {
            questionCtrl.deleteQuestionById(req.body.QuestionId);
            res.status(400).json({ errors: [txt.needMinChoicesForQuestion] });
        }
        else if(config.nbChoicesMax!==0 && choices.length>config.nbChoicesMax)
        {
            questionCtrl.deleteQuestionById(req.body.QuestionId);
            res.status(400).json({ errors: [txt.needMaxChoicesForQuestion+config.nbChoicesMax] });
        }
        else
        {
            for(let i in choices)
                await db["Choice"].create(choices[i], { fields: ["text", "isCorrect", "QuestionId"] });
            question=await questionCtrl.creaQuestionJson(req.body.QuestionId);// besoin de ces données pour la réponse
            await questionnaireCtrl.creaQuestionnaireJson(req.body.QuestionnaireId, true);// pour le cache + HTML
            questionnaire=await questionnaireCtrl.searchQuestionnaireById(req.body.QuestionnaireId, true);// nécessaire au réaffichage après ajout
            res.status(201).json({ message: txtQuestion.addOkMessage , questionnaire: questionnaire });
        }
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.messages)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}

exports.modify = async (req, res, next) =>
{
    try
    {             
        const db = require("../models/index");
        let question=await questionCtrl.searchQuestionById(req.params.id);
        if(!question)
            throw { message: txt.needQuestionForChoices+req.params.id };
        let choicesUpdated=[], choicesAdded=[], i=0, isCorrect, oneIsCorrect=false;
        while(!tool.isEmpty(req.body["choiceText"+i]))
        {
            if(req.body["choiceIsCorrect"+i]=="true")
            {
                isCorrect=true;
                oneIsCorrect=true;
            }
            else
                isCorrect=false;
            if(!tool.isEmpty(req.body["idChoice"+i]))
                choicesUpdated.push({ text:req.body["choiceText"+i], isCorrect:isCorrect, id:req.body["idChoice"+i] });
            else
                choicesAdded.push({ text:req.body["choiceText"+i], isCorrect:isCorrect, QuestionId:req.params.id });
            i++;
        }
        if(!oneIsCorrect)
            res.status(400).json({ errors: [txt.needOneGoodChoice] });
        else if(i<2)
            res.status(400).json({ errors: [txt.needMinChoicesForQuestion] });
        else if(config.nbChoicesMax!==0 && i>config.nbChoicesMax)
            res.status(400).json({ errors: [txt.needMaxChoicesForQuestion+config.nbChoicesMax] });
        else
        {
            let finded=false;
            for(let i in question.Choices)// = les réponses actuellement enregistrées
            {
                for(let j in choicesUpdated)
                {
                    if(choicesUpdated[j].id==question.Choices[i].id)
                    {
                        finded=true;
                        break;
                    }
                }
                if(!finded)
                    await db["Choice"].destroy( { where: { id : question.Choices[i].id }, limit:1 }); // ce choix n'a pas été gardé
                finded=false;
            }
            for(let i in choicesUpdated)
                await db["Choice"].update(choicesUpdated[i], { where: { id: choicesUpdated[i].id } , fields: ["text", "isCorrect"], limit:1 });
            for(let i in choicesAdded)
                await db["Choice"].create(choicesAdded[i], { fields: ["text", "isCorrect", "QuestionId"] });
            question=await questionCtrl.creaQuestionJson(req.params.id);// nécessaire d'attendre pour pouvoir tout retourner ensuite
            await questionnaireCtrl.creaQuestionnaireJson(req.body.QuestionnaireId, true);// pour le cache + HTML            
            questionnaire=await questionnaireCtrl.searchQuestionnaireById(req.body.QuestionnaireId, true);// nécessaire au réaffichage après enregistrement
            res.status(200).json({ message: txtQuestion.updateOkMessage , questionnaire: questionnaire });
        }
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.messages)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}