const config = require("../config/main.js");
const configLinks = require("../config/links.js");

const tool = require("../tools/main");
const toolError = require("../tools/error");

const questionnaireCtrl = require("./questionnaire");

const txt = require("../lang/"+config.adminLang+"/link");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

exports.create = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");        
        const questionnaire=await questionnaireCtrl.searchQuestionnaireById(req.body.QuestionnaireId);
        if(!questionnaire)
            throw { message:  txt.needQuestionnaire };
        else if(configLinks.nbLinksMax!==0 && questionnaire.Links.length>=configLinks.nbLinksMax)
            res.status(400).json({ errors: txt.needMaxLinksForQuestionnaire });
        else
        {
            const link=await db["Link"].create({ ...req.body }, { fields: ["url","anchor", "QuestionnaireId"] });
            questionnaireCtrl.creaQuestionnaireJson(req.body.QuestionnaireId);
            const questionnaireDatas=await questionnaireCtrl.creaQuestionnaireJson(req.body.QuestionnaireId);// me permet de retourner en réponse les infos actualisées pour les afficher
            res.status(201).json({ message: txt.addedOkMessage, questionnaire: questionnaireDatas });
        }
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.messages)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}

exports.modify = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");        
        const link=await searchLinkById(req.params.id);
        if(!link)
            res.status(404).json({ errors: txt.notFound });
        else
        {
            const questionnaire=await questionnaireCtrl.searchQuestionnaireById(link.QuestionnaireId);
            if(!questionnaire)
                throw { message: txt.needQuestionnaire };
            else if(req.connectedUser.User.status==="creator" && req.connectedUser.User.id!==questionnaire.Questionnaire.CreatorId)
                res.status(401).json({ errors: txtGeneral.notAllowed });            
            else
            {
                await db["Link"].update({ ...req.body }, { where: { id : req.params.id } , fields: ["url","anchor"], limit:1 });
                const questionnaireDatas=await questionnaireCtrl.creaQuestionnaireJson(link.QuestionnaireId);// me permet de retourner en réponse les infos actualisées pour les afficher
                res.status(200).json({ message: txt.updatedOkMessage, questionnaire: questionnaireDatas });
            }
        }
        next();
    }
    catch(e)
    {
        const returnAPI=toolError.returnSequelize(e);
        if(returnAPI.messages)
        {
            res.status(returnAPI.status).json({ errors : returnAPI.messages });
            next();
        }
        else
            next(e);
    }
}

exports.delete = async (req, res, next) =>
{
    try
    {
        const db = require("../models/index");       
        const link=await searchLinkById(req.params.id);
        if(!link)
            res.status(404).json({ errors: txt.notFound });
        else
        {
            const questionnaire=await questionnaireCtrl.searchQuestionnaireById(link.QuestionnaireId);
            if(!questionnaire)
                throw { message: txt.needQuestionnaire };
            else if(req.connectedUser.User.status==="creator" && req.connectedUser.User.id!==questionnaire.Questionnaire.CreatorId)
                res.status(401).json({ errors: txtGeneral.notAllowed });
            else
            {
                const nb=await db["Link"].destroy( { where: { id : req.params.id }, limit:1 });
                if(nb===1)
                {
                    const questionnaireDatas=await questionnaireCtrl.creaQuestionnaireJson(link.QuestionnaireId);
                    res.status(200).json({ message: txt.deletedOkMessage, questionnaire: questionnaireDatas });
                }
                else // ne devrait pas être possible, car déjà testé + haut !
                    throw { message: txt.needQuestionnaire };
            }
        }
        next();
    }
    catch(e)
    {
        next(e);
    }
}

exports.getOneById = async (req, res, next) =>
{
    try
    {
        const link=await searchLinkById(req.params.id);
        if(link)
            res.status(200).json(link);
        else
            res.status(404).json(null);
        next();
    }
    catch(e)
    {
        next(e);
    }
}


// FONCTIONS UTILITAIRES

const searchLinkById = async (id) =>
{
    const db = require("../models/index");
    const link = await db["Link"].findByPk(id);
    if(link)
        return link;
    else
        return false;
}