"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/payment");

module.exports = (sequelize, DataTypes) =>
{   // toutes les données viennent de WP, donc je ne maîtrise pas!
    const Payment = sequelize.define("Payment",
    {
        clientName:
        {
            type:DataTypes.STRING(255), allowNull: false,
            set(value)
            {
                value=tool.trimIfNotNull(striptags(value)).substring(0,255);
                this.setDataValue("clientName", value);
            },
            validate:
            {
                notNull: { msg: txt.needName }
            }
        },
        amount:
        {
            type: DataTypes.INTEGER(4).UNSIGNED, allowNull: false,
            validate:
            {
                notNull: { msg: txt.needAmount },
                isInt : { msg: txt.needCorrectAmount }
            }
        },
        codeCommande:
        {
            type:DataTypes.STRING(25), allowNull: false,
            set(value)
            {
                value=tool.trimIfNotNull(striptags(value)).substring(0,25);
                this.setDataValue("codeCommande", value);
            },
            validate:
            {
                notNull: { msg: txt.needCode }
            }
        }        
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    });
  Payment.associate = function(models)
  {
      Payment.belongsTo(models.User, { foreignKey: { name: "UserId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
  };
  return Payment;
};