"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/illustration");

module.exports = (sequelize, DataTypes) =>
{
    const Illustration = sequelize.define("Illustration",
    {
        url: // pas saisie, mais générée par "multer" lors du téléversement
        {
            type: DataTypes.STRING(255), allowNull: false,
            set(value) { this.setDataValue("url", striptags(value)); },
            unique:
            {
                args: true,
                msg: txt.needUniqueUrl
            },     
            comment: "Part of the url corresponding to the file name.",
            validate:
            {
                notNull: { msg: txt.needUrl },
                len:
                {
                    args: [5, 255],
                    msg: txt.needGoodLongUrl
                }
            }
        },
        alt:
        {
            type: DataTypes.STRING(255),
            set(value) { this.setDataValue("alt", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                len:
                {
                    args: [0, 255],
                    msg: txt.needGoodLongAlt
                }
            }
        },
        title:
        {
            type: DataTypes.STRING(255),
            set(value) { this.setDataValue("title", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                len:
                {
                    args: [0, 255],
                    msg: txt.needGoodLongTitle
                }
            }            
        },
        caption:
        {
            type: DataTypes.STRING(255),
            set(value) { this.setDataValue("caption", tool.trimIfNotNull(striptags(value, "<a>"))); }, // on accepte balise "a" car lien possible pour crédit image
            validate:
            {
                len:
                {
                    args: [0, 255],
                    msg: txt.needGoodLongCaption
                }
            }            
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    }
    );
    Illustration.associate = function(models)
    {
        Illustration.belongsTo(models.Questionnaire, { foreignKey: { name: "QuestionnaireId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
    };    
    return Illustration;
};