"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/pause");

module.exports = (sequelize, DataTypes) =>
{
    const Pause = sequelize.define("Pause",
    {
        name:
        {
            type: DataTypes.STRING(255), allowNull: false,
            set(value) { this.setDataValue("name", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needName },
                len:
                {
                    args: [1, 255],
                    msg: txt.needGoodLongName
                }
            }
        },
        startingAt:
        {
            type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW,
            validate:
            {
                notNull: { msg: txt.needStartingDate },
                isDate: { msg: txt.needValidStartingDate }
            }
        },
       endingAt:
        {
            type: DataTypes.DATE, allowNull: false,
            validate:
            {
                notNull: { msg: txt.needEndingDate },
                isDate: { msg: txt.needValidEndingDate },
                dateOk(value)
                {
                    if (value<=this.startingAt)
                        throw new Error(txt.needMinEndingDate);
                }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    }
    );
    Pause.associate = function(models)
    {
        Pause.belongsTo(models.Subscription, { foreignKey: { name: "SubscriptionId", allowNull: false, unique: true }, onDelete: "CASCADE", onUpdate: "CASCADE" });
    };    
    return Pause;
};