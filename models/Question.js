"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/question");

module.exports = (sequelize, DataTypes) =>
{
    const Question = sequelize.define('Question',
    {
        text:
        {
            type:DataTypes.STRING(255), allowNull: false,
            set(value) { this.setDataValue('text', tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needText },
                len:
                {
                    args: [1, 255],
                    msg: txt.needNotTooLongText
                }
            }
        },
        explanation:
        {
            type: DataTypes.TEXT,
            set(value) { this.setDataValue('explanation', tool.trimIfNotNull(striptags(value))); },
            comment: "Allows you to display explanation for this question after checking the user's answers."
        },
        rank:
        {
            type: DataTypes.INTEGER(2).UNSIGNED, allowNull: false, defaultValue:1,
            comment: "Ranking of the answer among those proposed.",
            validate:
            {
                notNull: { msg: txt.needNumberForRank },
                isInt: { msg: txt.needNumberForRank },
                min:
                {
                    args: [1],
                    msg: txt.needNumberForRank
                }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    });
    Question.associate = function(models)
    {
        Question.hasMany(models.Choice);
        Question.belongsTo(models.Questionnaire, { foreignKey: { name: "QuestionnaireId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
    };
    return Question;
};