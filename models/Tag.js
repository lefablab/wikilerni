"use strict";

const striptags = require("striptags");

const slugify = require('slugify');
const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/tag");

module.exports = (sequelize, DataTypes) =>
{
    const Tag = sequelize.define('Tag',
    {
        name:
        {
            type:DataTypes.STRING(100), allowNull: false,
            unique:
            {
                args: true,
                msg: txt.needUniqueName
            },
            set(value) { this.setDataValue('name', tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needName },
                len:
                {
                    args: [1, 100],
                    msg: txt.needNotTooLongName
                }
            }
        },
        slug:
        {
            type: DataTypes.STRING(100), allowNull: false,
            unique:
            {
                args: true,
                msg: txt.needUniqueUrl
            },
            set(value)
            {
                value=tool.trimIfNotNull(striptags(value));
                if(value!==null)
                    this.setDataValue("slug", slugify(value.substring(0,100), { lower:true, strict:true }));
                else if(this.name!==null)
                    this.setDataValue("slug", slugify(this.name.substring(0,100), { lower:true, strict:true }));
            },
            validate:
            {
                notNull: { msg: txt.needUrl }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    });
  Tag.associate = function(models)
  {
        Tag.belongsToMany(models.Questionnaire, { through: models.QuestionnaireClassification });
  };
  return Tag;
};