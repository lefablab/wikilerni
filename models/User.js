"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");
const configMail = require("../config/mail");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/user");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

module.exports = (sequelize, DataTypes) =>
{
    const User = sequelize.define("User",
    {
        name:
        {
            type: DataTypes.STRING(70),
            allowNull: false,
            set(value) { this.setDataValue("name", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needName },
                len:
                {
                    args: [1, 70],
                    msg: txt.needNotTooLongName
                }
            }
        },
        email:
        {
            type: DataTypes.STRING(255),
            allowNull: false,
            unique:
            {
                args: true,
                msg: txt.needUniqueEmail
            },
            set(value) { this.setDataValue("email", tool.trimIfNotNull(striptags(value))); },
            validate:
            {   
                notNull: { msg: txt.needEmail },
                isEmail: { msg: txt.needEmail+" "+txtGeneral.notValidFormat },
                len:
                {
                    args: [1, 255],
                    msg: txt.needNotTooLongEmail
                }
            }            
        },
        password:
        {
            type: DataTypes.STRING(255), allowNull: false,
            validate:
            {
                notNull: { msg: txt.needPassWord }
            }
        },
        status:
        {
            type: DataTypes.ENUM("admin", "manager", "creator", "user"), allowNull: false, defaultValue: "user",
            validate:
            {
                notNull: { msg: txt.needStatus },
                isIn:
                {
                    args: [["admin", "manager", "creator", "user"]],
                    msg: txt.needStatus+" "+txtGeneral.notValidFormat
                }
            }
        },
        language: // liste des langues dispo dans un fichier de configuration fourni par l'API au client
        {
            type: DataTypes.STRING(4), allowNull: false, defaultValue: "fr",
            set(value) { this.setDataValue("language", tool.trimIfNotNull(value)); },
            validate:
            {
                notNull: { msg: txt.needLanguage },
                len:
                {
                    args: [2, 4],
                    msg: txt.needLanguage+" "+txtGeneral.notValidFormat
                } // en fait, il faudra vérifier l'existence du fichier de la langue choisie.
            }
        },
        adminComments:
        {
            type: DataTypes.TEXT, allowNull: true,
            set(value) { this.setDataValue("adminComments", tool.trimIfNotNull(striptags(value))); },
            comment : "Not published."
        },        
        connectedAt:
        {
            type: DataTypes.DATE,
            validate:
            {
                isDate: { msg: txt.needValidLastConnectionDate }
            }// !! attention doit = null et pas seulement vide pour ne pas être rejeté, quand l'utilisateur n'a pas encore validé son compte
        },
        smtp:
        {
            type: DataTypes.INTEGER(1).UNSIGNED, allowNull: false, defaultValue: 0,
            comment : "Allows to assign a different SMTP server in case of deliverability issues.",
            validate:
            {
                notNull: { msg: txt.needSMTP },
                isInt: { msg: txt.needSMTP+" "+txtGeneral.notValidFormat },
                validSMTP(value)
                {
                    if(!configMail.SMTP.names[value])
                        throw new Error(txt.needSMTPNotFound);
                }
            }
        },
        timeDifference: // https://en.wikipedia.org/wiki/Time_zone
        {
            type: DataTypes.INTEGER(3), allowNull: false, defaultValue: 0,
            comment: "Time difference (in minutes) from UTC.",
            validate: // messages d'erreur à ne pas envoyer aux utilisateurs
            {
                notNull: { msg: txt.needTimeDifference },
                isInt: { msg: txt.needTimeDifference+" "+txtGeneral.notValidFormat },
                min:
                {
                    args: [-720],
                    msg: txt.needMinTimeDifference
                },
                max:
                {
                    args: [840],
                    msg: txt.needMaxTimeDifference
                }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    }
    );
    User.associate = function(models)
    {
        User.hasMany(models.Payment);
        User.hasMany(models.Questionnaire, { as: "Creator", foreignKey: { name: "CreatorId" } });
        User.hasMany(models.Answer);
        User.belongsTo(models.User, { as: "Godfather" });
        User.hasOne(models.Subscription);
        User.belongsToMany(models.Questionnaire, { through: models.QuestionnaireAccess });
    };
    return User;
};