"use strict";

module.exports = (sequelize, DataTypes) =>
{
    const QuestionnaireAccess = sequelize.define("QuestionnaireAccess",
    {
        selfCreatedOk:
        {
            type: DataTypes.BOOLEAN(1), allowNull: false, defaultValue: true,
            comment: "True if the user accesses the questionnaire on his own by answering it."
        }
    },
    {
    timestamps: true, 
    updatedAt: false
    });
  QuestionnaireAccess.associate = function(models)
  {
  
  };
  return QuestionnaireAccess;
};