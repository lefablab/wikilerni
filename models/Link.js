"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/link");

module.exports = (sequelize, DataTypes) =>
{
    const Link = sequelize.define("Link",
    {
        url:
        {
            type: DataTypes.STRING(255), allowNull: false,
            set(value) { this.setDataValue("url", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needUrl },
                isUrl: { msg: txt.needValidUrl },
                len:
                {
                    args: [0, 255],
                    msg: txt.needNotTooLongUrl
                }
            }            
        },
        anchor:
        {
            type:DataTypes.STRING(150), allowNull: false,
            set(value) { this.setDataValue("anchor", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needAnchor },
                len:
                {
                    args: [5, 150],
                    msg: txt.needGoodLongAnchor
                }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    });
    Link.associate = function(models)
    {
        Link.belongsTo(models.Questionnaire, { foreignKey: { name: "QuestionnaireId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
    };
    return Link;
};