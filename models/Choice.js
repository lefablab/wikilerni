"use strict";

const striptags = require("striptags");

const tool = require("../tools/main");
const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/choice");
const txtGeneral = require("../lang/"+config.adminLang+"/general");
// !! Attention, car on teste ici des données envoyées par l'application.
// Donc ne pas afficher les messages à l'utilisateur final, mais les journaliser.

module.exports = (sequelize, DataTypes) =>
{
    const Choice = sequelize.define("Choice",
    {
        text:
        {
            type:DataTypes.STRING(255), allowNull: false,
            set(value) { this.setDataValue("text", tool.trimIfNotNull(striptags(value))); },
            validate:
            {
                notNull: { msg: txt.needTextForChoice }, // mais si vide, le contrôleur ne lancera pas l'enregistrement
                len:
                {
                    args: [1, 255],
                    msg: txt.needGoodLongTextForChoice
                }  
            }
        },
        isCorrect:
        {
            type: DataTypes.BOOLEAN, allowNull: false, defaultValue:false,
            comment: "Necessary to designate the correct answers to a quiz.",
            validate:
            {
                notNull: { msg: txt.needKnowIsCorrectChoice },
                isIn:
                {
                    args: [[true, false]],
                    msg: txt.needKnowIsCorrectChoice+" "+txtGeneral.notValidFormat
                }
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    });
    Choice.associate = function(models)
    {
        Choice.belongsTo(models.Question, { foreignKey: { name: "QuestionId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
    };
    return Choice;
};