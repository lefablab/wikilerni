"use strict";

// revoir les requires pour ne prendre que ce j'ai besoin ?
const striptags = require("striptags");

const slugify = require("slugify");
const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/group");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

module.exports = (sequelize, DataTypes) =>
{
    const Group = sequelize.define("Group",
    {
        title:
        {
            type: DataTypes.STRING(255), allowNull: false,
            set(value)
            {
                this.setDataValue("title", tool.trimIfNotNull(striptags(value)));
            },
            validate:
            {
                notNull: { msg: txt.needTitle },
                len:
                {
                    args: [1, 255],
                    msg: txt.needNotTooLongTitle
                }         
            }
        },
        slug:
        {
            type: DataTypes.STRING(150), allowNull: false,
            unique:
            {
                args: true,
                msg: txt.needUniqueUrl
            },
            set(value)
            {
                value=tool.trimIfNotNull(striptags(value));
                if(value!==null)
                    this.setDataValue("slug", slugify(value.substring(0,150), { lower:true, strict:true }));
                else if(this.title!==null)
                    this.setDataValue("slug", slugify(this.title.substring(0,150), { lower:true, strict:true }));
            },
            validate:
            {
                notNull: { msg: txt.needUrl }
            }
        },
        introduction:
        {
            type: DataTypes.TEXT, allowNull: true,
            set(value) { this.setDataValue("introduction", tool.trimIfNotNull(striptags(value,"<p><b><i><em><strong><ul><li><div><a><br>"))); }
        },
        publishingAt:
        {
            type: DataTypes.DATE, comment: "If null, the questionnaire with the questions for each element of the group is not published, but its elements may be.",
            set(value) { this.setDataValue("publishingAt", tool.trimIfNotNull(value)); },
            validate:
            {   
                isDate: { msg: txt.needCorrectPublishingDate }
            }
        },
        language:
        {
            type: DataTypes.STRING(4), allowNull: false, defaultValue: "fr",
            validate:
            {
                notNull: { msg: txt.needLanguage },
                len:
                {
                    args: [2, 4],
                    msg: txt.needLanguage+" "+txtGeneral.notValidFormat
                } // en fait, il faudra vérifier l'existence du fichier de la langue choisie.
            }
        }
    },
    {
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    }
    );
    Group.associate = function(models)
    {
        Group.hasMany(models.Questionnaire);
        Group.hasMany(models.Answer);        
        Group.belongsTo(models.User, { as: "Creator", foreignKey: { name: "CreatorId", allowNull: false } });
    };
    return Group;
};