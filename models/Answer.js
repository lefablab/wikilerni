"use strict";

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/answer");
// !! Attention, car on teste ici des données envoyées par l'application.
// Donc ne pas afficher les messages à l'utilisateur final, mais les journaliser.

module.exports = (sequelize, DataTypes) =>
{
    const Answer = sequelize.define("Answer",
    {
        nbQuestions:
        {
            type: DataTypes.INTEGER(2).UNSIGNED, allowNull: false,
            comment: "The number of questions in the quiz may change between answers.",
            validate:
            {
                notNull: { msg: txt.needNumberUserResponses },
                isInt: { msg: txt.needIntegerNumberUserResponses },
                min:
                {
                    args: [1],
                    msg: txt.needMinNumberUserResponses
                },
                max:
                {
                    args: [100],// dans le contrôleur plutôt comparer au nombre de question du questionnaire.
                    msg: txt.needMaxNumberUserResponses
                }
            }
        },
        nbCorrectAnswers:
        {
            type: DataTypes.INTEGER(2).UNSIGNED, allowNull: false,
            validate:
            {
                notNull: { msg: txt.needNumberCorrectResponses },
                isInt: { msg: txt.needIntegerNumberCorrectResponses },
                min:
                {
                    args: [0],
                    msg: txt.needMinNumberCorrectResponses
                },
                moreAnswersThanQuestions(value)
                {
                    if(parseInt(value,10) > parseInt(this.nbQuestions,10))
                        throw new Error(txt.needMaxNumberCorrectResponses);
                }
            }            
        },
        duration:
        {
            type: DataTypes.INTEGER(5).UNSIGNED,
            validate:
            {
                isInt: { msg: txt.needIntegerNumberSecondesResponse },
                min:
                {
                    args: [0],
                    msg: txt.needMinNumberSecondesResponse
                }
            } 
        }
    },
    {
    timestamps: true,
    updatedAt: false
    });
  Answer.associate = function(models)
  {
      Answer.belongsTo(models.User, { foreignKey: { name: "UserId", allowNull: false }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
      Answer.belongsTo(models.Questionnaire, { foreignKey: { name: "QuestionnaireId", allowNull: true }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
      Answer.belongsTo(models.Group, { foreignKey: { name: "GroupId", allowNull: true }, onDelete: 'CASCADE', onUpdate: 'CASCADE' });
  };
  return Answer;
};