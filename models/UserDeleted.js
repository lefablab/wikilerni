"use strict";

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/userdeleted");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

module.exports = (sequelize, DataTypes) =>
{
    const UserDeleted = sequelize.define("UserDeleted",
    {
        createdAt:
        {
            type: DataTypes.DATE, allowNull: false,
            validate:
            {
                isDate: { msg: txt.needValidCreationDate }
            }
        },
        deletedAt:
        {
            type: DataTypes.DATE, allowNull: false,
            validate:
            {
                isDate: { msg: txt.needValidDeleteDate }
            }
        },
        wasValided:
        {
            type: DataTypes.BOOLEAN, allowNull: false, defaultValue:false,
            validate:
            {
                notNull: { msg: txt.needKnowIfWasValided },
                isIn:
                {
                    args: [[true, false]],
                    msg: txt.needKnowIfWasValided+" "+txtGeneral.notValidFormat
                }
            }
        },       
        wasPremium:
        {
            type: DataTypes.BOOLEAN, allowNull: false, defaultValue:false,
            validate:
            {
                notNull: { msg: txt.needKnowIfWasValided },
                isIn:
                {
                    args: [[true, false]],
                    msg: txt.needKnowIfWasValided+" "+txtGeneral.notValidFormat
                }
            }
        }  
    },
    {
        timestamps: false,
        charset: "utf8mb4",
        collate: "utf8mb4_unicode_ci"
    }
    );
    UserDeleted.associate = function(models)
    {
    };
    return UserDeleted;
};