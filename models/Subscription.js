"use strict";

const tool = require("../tools/main");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/subscription");
const txtGeneral = require("../lang/"+config.adminLang+"/general");

module.exports = (sequelize, DataTypes) =>
{
    const Subscription = sequelize.define("Subscription",
    {
        numberOfDays:
        {
            type: DataTypes.INTEGER(6).UNSIGNED, allowNull: false, defaultValue: 0,
            comment: "The duration in number of days of the subscription",
            validate:
            {
                notNull: { msg: txt.needNumberOfDays },
                isInt : { msg: txt.needIntegerNumberOfDays },
                min :
                {
                    args: [0],
                    msg: txt.needMinNumberOfDays
                }
            }
        },
        receiptDays:
        {
            type: DataTypes.STRING(7), defaultValue: config.defaultReceiptDays,
            comment: "Days on which the user has chosen to receive new questionnaires. The same numbers as in Mysql's DAYOFWEEK function.",
            set(value) { this.setDataValue("receiptDays", tool.trimIfNotNull(value)); },
            validate:
            {
                len:
                {
                    args: [1,7],
                    msg: txt.needNotTooLongDaysList
                },
                validDays(value)
                {
                    let daysNumber="1234567";
                    let cars=value.split(""), carsOk="";
                    for (let car of cars)
                    {
                        if(daysNumber.indexOf(car)===-1)
                            throw new Error(txt.needValidDaysList+car);
                        else if(carsOk.indexOf(car)!==-1)
                            throw new Error(txt.needUniqueDaysList+car);
                        carsOk+=car;
                    }
                }
            }
        },
        lastProcessingAt:
        {
            type: DataTypes.DATE, allowNull: false, defaultValue: "1970-01-01",
            comment: "Date of last subscription processing (sending quiz, etc.)."
        }
    },
    {
    }
    );
    Subscription.associate = function(models)
    {
        Subscription.belongsTo(models.User, { foreignKey: { name: "UserId", allowNull: false, unique: true }, onDelete: 'CASCADE', onUpdate: 'CASCADE' }),
        Subscription.hasMany(models.Pause)
    };
    return Subscription;
};