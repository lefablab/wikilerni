const config = require("../config/main.js");
const toolFile = require("../tools/file");

const txt = require("../lang/"+config.adminLang+"/general");

module.exports = async (req, res, next) =>
{
    try
    {
        // possibilité de bloquer si ip appel non acceptée ?  
        if (req.params.token !== config.cronToken)
        {
            const newCountFail={ ip: req.ip, lastTime: new Date() }; // utile vu qu'il y a les logs généraux ?
            let logsCronFail=await toolFile.readJSON(config.dirTmp, "cronfails");
            if(!logsCronFail)
                logsCronFail=[newCountFail];
            else
                logsCronFail.unshift(newCountFail);
            await toolFile.createJSON(config.dirTmp, "cronfails", logsCronFail);
            throw { message:txt.failAuthCron, status:401 };
        }
        else
            next();
    }
    catch(e)
    {
        next(e);
    }
};