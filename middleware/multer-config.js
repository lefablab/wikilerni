const multer = require("multer");
const slugify = require('slugify');
const fs = require("fs-extra");

const configIllustrations = require("../config/illustrations.js");

const config = require("../config/main.js");
const txt = require("../lang/"+config.adminLang+"/illustration");

const storage = multer.diskStorage(
{
    destination: (req, file, callback) =>
    {
        callback(null, configIllustrations.dirIllustrationsTmp);// sera ensuite redimensionnée dans son répertoire final
    },
    filename: (req, file, callback) =>
    {
        // par défaut, multer créé un nom de fichier unique, mais aléatoire et sans extension.
        let name=file.originalname;        
        let extension = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
        name=slugify(name.substring(0, name.lastIndexOf(".")).substring(0, 230), { lower:true });
        name=name+"-"+Date.now()+"."+extension;
        callback(null, name);
    }
});

const fileFilter = (req, file, callback) =>
{
    if(config.mimeTypesForIllustration.indexOf(file.mimetype) ===-1)
    {
        callback(null, false);
        req.body.fileError=txt.needGoodFile;
    }
    else
        callback(null, true);
}

try
{
    module.exports = multer({storage: storage, fileFilter}).single("image");
    // -> indique que nous permettons uniquement le téléversement d'un fichier via un champ nommé "image".
    // Même si plusieurs illustrations sont possibles, elles seront téléversées et enregistrées séparément.
    //  module.exports = multer({storage: storage, limits : {fileSize:... }, fileFilter}).single("image"); -> pas possible d'intercepter l'erreur si fichier trop gros ?
}
catch(e)
{
    next(e);
}