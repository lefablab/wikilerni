// Pour l'instant se contente d'initialiser la durée de certains scripts...
const config = require("../config/main.js");

const txt = require("../lang/"+config.adminLang+"/general");

module.exports = async (req, res, next) =>
{
    try
    {
        req.timeBegin=Date.now();// bug étrange quand appelé dans app.js, ce qui aurait été + simple...
        next();
    }
    catch(e)
    {
        res.status(500).json({ errors: txt.serverError });
        next(e);
    }
};