# Projet WikiLerni

## Introduction

Vous trouverez sur ce dépôt le code source du projet WikiLerni que vous pouvez visiter sur cette page : https://www.wikilerni.com

Le backend est développé avec Node.js v14/Express.
Le frontend en "Vanilla.js" + Babel.js pour gérer les anciens navigateurs.

Le code est fourni tel quel, sachant qu'il reste encore beaucoup de choses à faire et qu'il s'agit d'un développement spécifique pas forcément réutilisable en entier par ailleurs. Une partie du code peut être réutilisée pour un générateur de quizs de type QCM par exemple. À vous de voir.

Les quizs et articles du site WikiLerni sont aussi libres (CC BY-SA 3.0) et les quizs sont utilisables de manière autonome en enregistrant le fichier html localement. Seul l'enregistrement des résultats nécessite l'appel au serveur node.js.

## Installation

Il n'y a pas de script d'installation pour l'instant.
Mais si vous souhaitez tenter une installation sur votre machine (je ne promets rien !), voici la procédure à suivre :

- adapter le fichier example.env et renommer le : .env
- adapter le fichier config/instance-example.js et renommer le : instance.js
- créer une base de données vierge à l'aide du fichier models/wikilerni-crea.sql (doit fonctionner avec MySQL et MariaDB).
- installer les dépendances avec "npm install"
- lancer le serveur node + un serveur front pointant le répertoire /front/public/
- créer un premier compte utilisateur avec /front/public/inscription.html et changer son statut directement dans la base de données pour "admin" ou "manager" pour lui permettre de publier des quizs, gérer les comptes utilisateurs, etc.
- l'enregistrement du premier article créera la page d'accueil du site "/front/public/index.html" (entre autres).