module.exports =
{
    "appenders":
    {
        "fileLogs":
        {
            "type": "dateFile",
            "filename": "logs/day.log",
            "alwaysIncludePattern" : true,
            "numBackups": 7,
            "keepFileExt": true  
        },
        "console": { "type": "console" }
    },
    "categories":
    {
        "production": { "appenders": ["fileLogs"], "level": "trace" },
        "default": { "appenders": [ "console" ], "level": "trace" }
    }
}