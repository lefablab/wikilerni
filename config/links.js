module.exports =
{
    // API'routes (after "apiUrl" defined in instance.js)
    linksRoute: "/link/",
    // forms : à compléter avec valeurs par défaut, etc. cf modèle
    Link :
    {
        url: { maxlength: 255, required: true },
        anchor: { maxlength: 150, required: true }
    }
};