/// À ADAPTER ET RENOMMER : instance.js.

const users = require("./users");
const questionnaires = require("./questionnaires");

module.exports =
{
    apiUrl: "https://...",
    siteUrl: "https://...",
    adminName: "bob",
    adminEmail: "bob@example.tld",
    senderName: "bob",
    senderEmail: "bob@example.tld",
    adminLang: "fr",
    theme: "wikilerni", // le thème utilisé (dans /views) pour générer les pages HTML. Contient ses propres fichiers de configuration.
    availableLangs: ["fr"],// Languages in which the site is available. The first one is the default one.
    siteName: "WikiLerni",
    beginCodeGodfather: "WL", // case-sensitive and can't contain "@" !
    defaultReceiptDays: "1234567", // Par défaut, quel(s) jour(s) de la semaine, l'utilisateur reçoit-il quelque chose ? (1=dimanche, 2=lundi... 7=samedi)    
    cronTimingAlertInSeconde: 120, // for logs
    responseTimingAlertInSeconde: 3, // idem
    tokenSignupValidationTimeInHours: "48h", // see : https://github.com/zeit/ms
    tokenLoginLinkTimeInHours: "1h",
    tokenConnexionMinTimeInHours: "24h",
    tokenConnexionMaxTimeInDays: "180 days",
    tokenLoginChangingTimeInHours: "1h",// for email & password changing
    tokenDeleteUserTimeInHours: "1h",
    tokenUnsubscribeLinkTimeInDays: "7 days", // token send with subscription's emails
    freeAccountTimingInDays: 15,// if 0 = unlimited
    freeAccountExpirationNotificationInDays: 3,
    accountExpirationFirstNotificationInDays: 10,
    accountExpirationSecondNotificationInDays: 3,
    inactiveAccountTimeToDeleteInDays: 180,
    // Questionnaires:
    nbQuestionsMin: 1, // minimum number of questions for the questionnaire to be publishable
    nbQuestionsMax: 0, // if 0 = not maximum
    nbChoicesMax: 10,
    nbNewQuestionnaires: 10, // for RSS, etc.
    hourGiveNewQuestionnaireBegin: 3, // in user local time
    hourGiveNewQuestionnaireEnd: 8, // idem
    maxQuestionnaireSendedAtSameTime: 50, // for subscription's e-mailing
    minSearchQuestionnaires: 3,
    fieldNewQuestionnaires : "publishingAt", // field to be used to create the list of the last questionnaires, can be "createdAt", "updatedAt" or "publishingAt"
    // Groups :
    nbQuestionnairesByGroupMin: 2,
    nbQuestionnairesByGroupMax: 0,    
    // Illustrations:
    nbIllustrationsMin: 0,
    nbIllustrationsMax: 1,
    maxIllustrationSizeinOctet: 1000000,// Not checked yet. To be continued.
    mimeTypesForIllustration: [ "image/jpg", "image/jpeg", "image/png", "image/gif", "image/png" ],
    // -- Upload and resize:
    illustrationsWidthMaxInPx: 400,
    illustrationsMiniaturesWidthMaxInPx: 200,
    // Links:
    nbLinksMin: 1,
    nbLinksMax: 1,
    // Questions & responses:
    nbQuestionsMin: 1,
    nbQuestionsMax: 0,
    nbChoicesMax: 10,
    // à supprimer quand tous les "require" à jour:
    passwordMinLength: users.password.minlength,
    dirCacheUsers: users.dirCacheUsers,
    dirCacheUsersAnswers: users.dirCacheUsersAnswers,
    dirCacheQuestionnaires: questionnaires.dirCacheQuestionnaires,
    dirCacheQuestions: questionnaires.dirCacheQuestions,
    dirCacheUsersQuestionnaires: questionnaires.dirCacheUsersQuestionnaires,
    dirHTMLQuestionnaires: questionnaires.dirHTMLQuestionnaires,
    dirWebQuestionnaires: questionnaires.dirWebQuestionnaires
};