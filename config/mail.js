require('dotenv').config();
const instance = require("./instance");

module.exports =
{
    "SMTP" :
    {
        "names": process.env.SMTP_NAMES.split(","),
        "hosts": process.env.SMTP_HOSTS.split(","),
        "ports": process.env.SMTP_PORTS.split(","),
        "secures" : process.env.SMTP_SECURES.split(","),
        "logins" : process.env.SMTP_LOGINS.split(","),
        "passwords" : process.env.SMTP_PASSWORDS.split(",")
    },
    "SENDER" :
    {
        "name" : instance.senderName,
        "email" : instance.senderEmail
    }
};