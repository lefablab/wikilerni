const instance = require("./instance");

module.exports =
{
    // API'routes (after "apiUrl" defined in instance.js)
    illustrationsRoute: "/illustration/",
    // forms : à compléter avec valeurs par défaut, etc. cf modèle
    Illustration :
    {
        alt: { maxlength: 255 },
        title: { maxlength: 255 },
        caption: { maxlength: 255 },
        image: { required: true, accept: instance.mimeTypesForIllustration.join(",") }
    },
    // files upload tempory dir
    dirIllustrationsTmp : "temp",
    dirIllustrations: "front/public/www/img/quizs"
};