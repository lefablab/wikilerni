require('dotenv').config();

const instance = require("./instance");

instance.env=process.env.NODE_ENV;
instance.bcryptSaltRounds=parseInt(process.env.BCRYPT_SALT_ROUNDS,10);
instance.cronToken=process.env.CRON_TOKEN;
instance.tokenPrivateKey=process.env.TOKEN_PRIVATE_KEY;
instance.maxLoginFail=parseInt(process.env.MAX_LOGIN_FAILS,10);
instance.loginFailTimeInMinutes=parseInt(process.env.LOGIN_FAIL_TIME_IN_MINUTES,10);
instance.dirCache="datas";
instance.dirHTML="front/public/www";
instance.dirTmp="datas/tmp";
instance.dirTmpLogin="datas/tmp/logins";

module.exports = instance;