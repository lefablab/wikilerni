module.exports =
{    
    // API'routes (after "apiUrl" defined in instance.js)
    questionnaireRoutes: "/questionnaire",// la base à laquelle s'ajoute les routes suivantes
    getListNextQuestionnaires: "/getlistnextquestionnaires/",
    getQuestionnaireRoutes: "/get",
    getRandomQuestionnairesRoute : "/getrandom", 
    getStatsQuestionnaires : "/stats/",
    previewQuestionnaireRoutes: "/preview",
    publishedQuestionnaireRoutes: "/quiz/",
    regenerateHTML: "/htmlregenerated",
    searchAdminQuestionnairesRoute : "/searchadmin",
    searchQuestionnairesRoute : "/search",
    // -- groupes :
    groupRoutes: "/group",
    getGroupRoute: "/get/",
    previewGroupRoutes: "/preview",
    searchGroupsRoute : "/search",
    // -- questions & choices :
    questionsRoute: "/question/",
    // -- tags :
    tagsSearchRoute: "/tags/search/",
    // -- answers :
    getAdminStats: "/getadminstats/",
    getPreviousAnswers: "/user/answers/",
    getStatsAnswers : "/user/anwswers/stats/",// fonctionne aussi pour les groupes
    saveAnswersRoute: "/answer/",// idem
    // forms : à compléter avec valeurs par défaut, etc. cf modèle
    Questionnaire :
    {
        title: { maxlength: 255, required: true },
        slug: { maxlength: 150 }, // champ requis mais calculé à partir du titre qd laissé vide dans le formulaire
        introduction: { required: true }
    },
    searchQuestionnaires : { minlength: 3, required: true },
    Group :
    {
        title: { maxlength: 255, required: true },
        slug: { maxlength: 150 }, // champ requis mais calculé à partir du titre qd laissé vide dans le formulaire
    },
    Question :
    {
        text: { maxlength: 255, required: true },
        rank: { required: true, min:1, defaultValue:1 }
    },          
    Choice :
    {
        text: { maxlength: 255, required: true }
    },
    search: { minlength: 3, required: true },
    searchGroups: { minlength: 3, required: true },
    // Emplacement des fichiers JSON générés :
    dirCacheGroups : "datas/questionnaires/groups",
    dirCacheQuestionnaires : "datas/questionnaires",
    dirCacheQuestions : "datas/questionnaires/questions",
    dirCacheTags : "datas/questionnaires/tags",
    dirCacheUsersQuestionnaires : "datas/users/questionnaires",
    // Emplacement des fichiers HTML générés :
    dirHTMLGroups : "front/public/www/quiz/gp",
    dirHTMLQuestionnaires : "front/public/www/quiz",
    dirHTMLNews : "front/public/www/quizs",
    dirHTMLTags : "front/public/www/quizs",
    // Idem mais pour urls :
    dirWebGroups : "quiz/gp",
    dirWebQuestionnaires : "quiz",
    dirWebNews : "quizs/",
    dirWebTags : "quizs/",
    // limite des résultat du moteur de recherche, quand demande de résultats au hasard :
    nbRandomResults : 3,
    /* Valeurs en fait définies dans instance.js donc à supprimer quand plus utilisées ailleurs : */
    nbQuestionsMin: 1,
    nbQuestionsMax: 0,
    nbChoicesMax: 10,
    nbTagsMin: 0,
    nbTagsMax: 0, // 0 = not max    
};