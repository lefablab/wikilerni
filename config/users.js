module.exports =
{
    // API'routes (after "apiUrl" defined in instance.js)
    userRoutes: "/user",
    checkDeleteLinkRoute: "/confirmdelete/",   
    checkIfIsEmailfreeRoute: "/isemailfree",
    checkLoginRoute: "/checklogin/",
    checkNewLoginLinkRoute: "/confirmnewlogin/",
    checkSubscribeTokenRoute: "/validation/",
    connectionRoute: "/login",
    connectionWithLinkRoute: "/checkloginlink",
    createUserRoute: "/create",
    getAdminStats: "/getadminstats/",
    getGodChilds: "/getgodchilds/",    
    getGodfatherRoute: "/getgodfatherid",
    getLoginLinkRoute: "/getloginlink",
    getPayments: "/payment/getforoneuser/",
    getUserInfos: "/get/",
    getUsersQuestionnairesRoute: "/getusersquestionnaires/",// les questionnaires auxquels l'utilisateur a déjà eu accès via son abonnement    
    searchUserRoute: "/search/",
    signupCompletionRoute: "/signupcompletion/",
    subscribeRoute: "/signup",
    unsubscribeRoute: "/subscription/stop/",
    updateUserInfos: "/modify/",
    validateUserRoute: "/validate/",
    // forms : à compléter avec valeurs par défaut, etc. cf modèle
    name: { maxlength: 70, required: true },
    email: { maxlength: 255, required: true },
    password: { minlength: 8, maxlength:72, required: true }, // https://www.npmjs.com/package/bcrypt#security-issues-and-concerns
    newPassword: { minlength: 8, maxlength:72 },
    codeGodfather: { maxlength: 255 },
    cguOk: { value: "true", required: true },
    search: { minlength: 1, required: true },
    timeDifferenceMin: -720,
    timeDifferenceMax: 840,
    // JSON dir
    dirCacheUsers : "datas/users",
    dirCacheUsersAnswers : "datas/users/questionnaires/answers",
    dirCacheUsersWithoutAnswers : "datas/users/questionnaires/without"
};
