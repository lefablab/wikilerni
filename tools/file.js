// Quelques fonctions utiles créer ou lire les fichiers
const fs = require("fs-extra");

class ToolFile
{
    // Reçoit un objet et l'enregistre dans un fichier sous forme de chaîne
    static async createJSON(dir, file, obj)
    {
        try
        {
            let myFile=""+file;// "file" peut être un nombre = bug avec endsWith
            myFile=(myFile.endsWith(".json")) ? myFile : myFile+".json";
            const path=dir+"/"+myFile;
            await fs.outputJson(path, obj);// créé récursivement les répertoires si besoin
            return true;
        }
        catch (err)
        {
            console.error(err);
            return false;
        }
    }
    
    // Reçoit du code HTML et l'enregistre dans un fichier
    static async createHTML(dir, file, html)
    {
        try
        {
            let myFile=""+file;// "file" peut être un nombre = bug avec endsWith
            myFile=(myFile.endsWith(".html")) ? myFile : myFile+".html";
            const path=dir+"/"+myFile;
            await fs.outputFile(path, html);// créé récursivement les répertoires si besoin
            return true;
        }
        catch (err)
        {
            console.error(err);
            return false;
        }
    }
    
    // Reçoit du code XML et l'enregistre dans un fichier (à mutualiser avec précédente)
    static async createXML(dir, file, xml)
    {
        try
        {
            let myFile=""+file;// "file" peut être un nombre = bug avec endsWith
            myFile=(myFile.endsWith(".xml")) ? myFile : myFile+".xml";
            const path=dir+"/"+myFile;
            await fs.outputFile(path, xml);// créé récursivement les répertoires si besoin
            return true;
        }
        catch (err)
        {
            console.error(err);
            return false;
        }
    }

    // Vérifie si un fichier existe dans un répertoire
    static async checkIfFileExist(dir, file)
    {
        try
        {
            const path=dir+"/"+file;
            const fileInfos=await fs.stat(path);
            return true;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    }

    // Retourne un objet à partir d'un json contenu dans un fichier
    static async readJSON(dir, file)
    {
        try
        {
            let myFile=""+file;
            myFile=(myFile.endsWith(".json")) ? myFile : myFile+".json";
            const path=dir+"/"+myFile;
            const obj=await fs.readJson(path);
            return obj;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    }

    // Suppprime un fichier json, si il existe
    static async deleteJSON(dir, file)
    {
        try
        {
            let myFile=""+file;
            myFile=(myFile.endsWith(".json")) ? myFile : myFile+".json";
            const path=dir+"/"+myFile;
            await fs.remove(path);
            return true;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    }

    // Suppprime un fichier si il existe
    static async deleteFile(dir, file)
    {
        try
        {
            let myFile=""+file;
            const path=dir+"/"+myFile;
            await fs.remove(path);
            return true;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    } 

    static async deleteOldFilesInDirectory(dir, expirationTime)
    {
        try
        {
            let path, fileInfos;
            const files = await fs.readdir(dir);
            for (let i in files)
            {
                path=dir+"/"+files[i];
                fileInfos=await fs.stat(path);
                if(fileInfos.isFile() && fileInfos.mtimeMs<expirationTime)
                    await fs.remove(path);
            }
            return true;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    }

    static async deleteFilesInDirectory(dir, saveFiles)
    {
        try
        {
            let path, fileInfos;
            const files = await fs.readdir(dir);
            for (let i in files)
            {
                path=dir+"/"+files[i];
                fileInfos=await fs.stat(path);
                if(fileInfos.isFile() && saveFiles.indexOf(files[i])===-1)
                    await fs.remove(path);
            }
            return true;
        }
        catch(e)
        {
            if (e.code !== 'ENOENT')
                console.error(e);
            return false;
        }
    }
}

module.exports = ToolFile;