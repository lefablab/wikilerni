class ToolError
{
    // fournit le code et le(s) message(s) d'erreur à renvoyer au client suivant les cas + affiche/enregistre certaines infos dans les logs.
    static returnSequelize(error)
    {
        let returnAPI=[], errorsReturn=[];
        if(error.name==="SequelizeValidationError" || error.name==="SequelizeUniqueConstraintError")
        {
            for (let i = 0; i < error.errors.length; i++)
                errorsReturn.push(error.errors[i].message);
            returnAPI.status=400;
            returnAPI.messages=errorsReturn;
        }
        return returnAPI;
    }
}

module.exports = ToolError;