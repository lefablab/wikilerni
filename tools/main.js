// Quelques fonctions utiles pour les chaînes

class Tool
{
    static isEmpty(myVar)
    {
        if(myVar===undefined || myVar===null)
            return true;
        else
        {
            myVar+="";// si autre chose qu'une chaîne envoyé...
            myVar=myVar.trim();
            if(myVar==="")
                return true;
            else
                return false;
        }
    }
    
    static trimIfNotNull(myString)
    {
        if(Tool.isEmpty(myString))
            myString=null;
        else
        {
            myString+="";// si autre chose qu'une chaîne envoyé...
            myString=myString.trim();
        }
        return myString;
    }


    static shortenIfLongerThan(myString, max)
    {
        myString+="";// au cas où cela ne serait pas une chaîne...
       if(myString.length > max)
            myString=myString.substring(0, (max-3))+"…";
        return myString;
    }

    // source : https://stackoverflow.com/questions/15604140/replace-multiple-strings-with-multiple-other-strings
    static replaceAll(myString, mapObj)
    {
        const replaceElts = new RegExp(Object.keys(mapObj).join("|"),"gi");
        return myString.replace(replaceElts, (matched) =>
        {
            return mapObj[matched];
        });
    }

    // source : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Math/random
    static getRandomInt(min, max)
    {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    // à compléter : https://en.wikipedia.org/wiki/Date_format_by_country
    static dateFormat(dateString, lang="fr")
    {
        if(Tool.isEmpty(dateString))
            return "";
        let myDate=new Date(dateString);
        let myDay=myDate.getDate()+"";
        if(myDay.length===1)
            myDay="0"+myDay;
        let myMounth=(myDate.getMonth()+1)+"";
        if(myMounth.length===1)
            myMounth="0"+myMounth;
        let myYear=myDate.getFullYear();
        if(lang==="fr")
            return myDay+"/"+myMounth+"/"+myYear;
        else if (lang==="form")// 2014-02-09
            return myYear+"-"+myMounth+"-"+myDay;
        else
            return myMounth+"/"+myDay+"/"+myYear;
    }

    // On enlève volontairement les 0/O pour éviter les confusions !
    // Et mieux vaut aussi débuter et finir par une lettre simple.
    static getPassword (nbCarMin, nbCarMax)
    {
        const nbCar=nbCarMin+Math.floor(Math.random()*(nbCarMax-nbCarMin));
        const letters="ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
        const others="123456789!?.*-_%@&ÉÀÈÙ€$ÂÊÛÎ";
        let password=letters[Math.floor(Math.random()*letters.length)];
        for(let i=1;i<(nbCar-1);i++)
        {
            if((i % 2) ===1)
                password+=others[Math.floor(Math.random()*others.length)];
            else
                password+=letters[Math.floor(Math.random()*letters.length)];   
        }
        password+=letters[Math.floor(Math.random()*letters.length)];
        return password;
    }
}

module.exports = Tool;