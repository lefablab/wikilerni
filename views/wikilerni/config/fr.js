module.exports =
{
    // liens de l'interface
    headLinks:
    [
        { anchor: "Contact", attributes: { href:"/contact.html", rel: "nofollow" } },
        { anchor: "Mes quizs", attributes: { href:"/mes-quizs.html", rel: "nofollow", title:"Liste des quizs auxquels vous avez déjà répondu." } },
        { anchor: "Parcourir", attributes: { href:"/quizs/", id:"indexHeadLink", title:"Les dernières publications" } },
        { anchor: "À propos", attributes: { href:"/a-propos.html", title:"En savoir + sur WikiLerni" } },
        { anchor: "Accueil", attributes: { href:"/", title:"Page d'accueil" } }
    ],
    footLinks:
    [
        { anchor: "Blog", attributes: { href:"https://diaspora-fr.org/people/815767c0c09e0139ec6f32a01d0dfba2", title:"Le blog WikiLerni sur diaspora*" } },
        { anchor: "Crédits", attributes: { href:"/credits.html", title:"Qui a créé WikiLerni ? Quels sont vos droits ?" } },
        { anchor: "Mentions légales", attributes: { href:"/mentions-legales.html", rel: "nofollow" } },
        { anchor: "Données personnelles", attributes: { href:"/donnees.html", title:"Vos données personnelles sur WikiLerni" } },
        { anchor: "CGV & CGU", attributes: { href:"/CGV-CGU.html", rel: "nofollow" } }
    ],
    accountPage: "compte.html",
    aboutPage: "a-propos.html",
    adminHomePage: "admin.html",
    cguPage: "CGV-CGU.html",
    connectionPage : "connexion.html",
    deleteLinkPage : "aurevoir.html?t=",
    loginLinkPage : "login.html?t=",
    managerHomePage : "gestion.html",
    newLoginLinkPage : "newlogin.html?t=",
    questionnairesManagementPage: "gestion-quizs.html",    
    stopMailPage : "stop-mail.html?t=",
    subscribePage : "inscription.html",
    updateAccountPage: "compte.html",
    userHomePage : "mes-quizs.html",
    userHomePageTxt : "Tous mes quizs.",
    usersManagementPage: "gestion-utilisateurs.html",
    validationLinkPage : "validation.html?t=",
    /* Textes (général) */
    siteSlogan: "Cultivons notre jardin !",
    noJSNotification: "Désolé, mais pour l'instant, l'utilisation de WikiLerni nécessite l'activation du JavaScript.",
    mailRecipientTxt: "Message envoyé à :",
    licenceTxt: "@copyleft Le contenu de WikiLerni <a href=\"/credits.html\" title=\"En savoir plus ?\">est libre</a> et vous est offert sans publicité. Vous pouvez <a href=\"/participer-financement.html\" title=\"Financement participatif avec contre-parties\">participer à son financement en cliquant ici</a>.",
    /* Page d'accueil */
    homePageTxt: "Page d’accueil",
    homeTitle1: "De nature curieuse ?",
    homeP1: "<b>Avec WikiLerni, vous apprenez de nouvelles choses</b> et testez votre capacité d’attention.<br>Vous découvrez de courts articles, sur des sujets variés et lisibles en quelques minutes.<br><b>Des quizs vous permettent ensuite de tester ce que vous avez retenu</b>.<br>De nouvelles <b>graines de culture</b> sont ainsi semées dans votre jardin</b>.",
    homeTitle2: "La culture générale en liberté",
    homeP2: "<b>Pas de faits alternatifs</b>, tous les contenus proposés sont <b>sourcés par des articles Wikipédia</b>.<br>Et tout comme sur Wikipédia, le logiciel et le contenu publié sur WikiLerni <a href=\"/credits.html\" title=\"En savoir plus sur ce sujet\">sont partagés sous licences libres</a>.<br>Le tout sans publicité, ni commercialisation de vos données.<br><b>Sur WikiLerni, vous cultivez votre jardin en toute tranquillité.</b>",
    homeBtnAboutTxt: "En savoir plus sur WikiLerni ?",
    homeBtnSubscribeTxt:  "Testez WikiLerni",
    homeSubcriptionFormTitle:  "Recevez les prochains articles WikiLerni",
    /* Page dernières publications... */   
    newQuestionnairesTitle: "Culture générale - apprenez de nouvelles choses avec WikiLerni",
    newQuestionnairesIntro: "WikiLerni : testez vos connaissances et apprenez de nouvelles choses avec WikiLerni.",
    newsListTitle: "<p><b>Avec WikiLerni, vous pouvez toujours apprendre quelque chose de nouveau</b><br>Si dessous les dernières publications. Vous pouvez aussi <a href='/quizs/themes.html'>parcourir le site par thèmes ou mots-clés</a>.</p>",
    /* Plan du site, liste des tags */   
    tagListTitle: "Culture générale - des articles et quizs sur de nombreux thèmes !",
    tagListMetaDesc: "WikiLerni : découvrir les différents thèmes abordés par WikiLerni. Index du site.",
    tagListIntro: "<h3>Avec WikiLerni, devenez fort en thèmes... Oui mais quels thèmes ? :)</h3><blockquote>Aristote : « L’homme a naturellement la passion de connaître… »</blockquote>",
    /* Page quizs */
    answersExplanationsLinkText: "Relire",
    quizElementLinksIntro: "En savoir plus",
    quizElementSubcriptionFormTitle: "Recevez les prochains articles WikiLerni",
    explanationTitle: "Vous découvrez WikiLerni ?",
    explanationTxt: "<p>Le principe est simple : <b>vous commencez par lire l’article Wikipédia dont le lien vous est proposé</b>. Puis vous <b>afficher le quiz pour vérifier ce que vous avez retenu de votre lecture</b>. Suivant les questions, <b>une ou plusieurs réponses peuvent être correctes</b> et doivent donc être cochées. C’est toujours <b>le contenu de l’article Wikipédia qui fait foi</b> concernant les « bonnes » réponses. C’est une façon de tester à la fois votre capacité d’attention et votre mémoire. Les articles de Wikipédia peuvent évoluer, donc n’hésitez pas <a href='/contact.html'>à nous signaler une erreur</a>.</p><p><b>WikiLerni vous propose d’autres solutions pour améliorer votre culture générale</b>. Pour en savoir plus, cliquez sur le bouton ci-dessous.</p>",
    explanationElementTxt: "<p>WikiLerni vous propose de <b>découvrir de courts articles lisibles en quelques minutes</b> et portant sur des sujets très variés de <b>culture générale</b> (arts, histoire, littérature, sciences, etc.).</p><p>Ces articles sont basés sur <b>une ou plusieurs pages de Wikipédia</b> (fournies en lien), dont <b>ils extraient certaines informations</b>.</p><p>Chaque série d’articles est <b>suivie d’un quiz</b> permettant de tester ce que vous en avez retenu.</p><p><b>Vous apprenez ainsi régulièrement de nouvelles choses</b> très simplement.</p>",
    /* Autres */
    illustrationDir : "/img/quizs/",
    twitterAccount: "WikiLerni",
    maxQuestionnairesByPage: 12,
    maxQuestionnairesFeed: 5,
    maxQuestionnairesSiteHomePage: 3,
    nbQuestionnairesUserHomePage : 3,
};