const express = require("express");
const router = express.Router();

const auth = require("../middleware/auth");
const authAdmin = require("../middleware/authAdmin");

const userCtrl = require("../controllers/user");
const subscriptionCtrl = require("../controllers/subscription");

router.post("/getgodfatherid", userCtrl.getGodfatherId);
router.post("/isemailfree", userCtrl.checkEmailIsFree);
router.post("/signup", userCtrl.signup);
router.get("/validation/:token", userCtrl.signupValidation);
router.get("/checklogin/:token", userCtrl.checkToken);
router.post("/login", userCtrl.login);
router.post("/getloginlink", userCtrl.getLoginLink);
router.post("/checkloginlink", userCtrl.checkLoginLink);

// à tester après envoi d'une suggestion de lecture
router.get("/subscription/stop/:token", subscriptionCtrl.unsubscribeLink);

// routes nécessitant d'être connecté :
router.put("/modify/:id", auth, userCtrl.modify);
router.put("/signupcompletion/:id", auth, userCtrl.signUpCompletion);
router.post("/create", authAdmin, userCtrl.create);
router.post("/validate/:id", authAdmin, userCtrl.validate);// validation d'un user par un admin
router.get("/confirmnewlogin/:token", auth, userCtrl.checkNewLoginLink);
router.delete("/:id", auth, userCtrl.delete);
router.get("/confirmdelete/:token", auth, userCtrl.checkDeleteLink);
router.get("/getgodchilds/", auth, userCtrl.getOneUserGodChilds);
router.get("/getgodchilds/:id", authAdmin, userCtrl.getOneUserGodChilds);
router.get("/get/:id", auth, userCtrl.getOneUserById);
router.post("/search", authAdmin, userCtrl.searchUsers);
router.get("/getadminstats", authAdmin, userCtrl.getStats);
router.get("/getusersquestionnaires/:id/:begin/:nb/:output", auth, subscriptionCtrl.getQuestionnairesForUser);

module.exports = router;