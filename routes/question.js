const express = require("express");
const router = express.Router();

const auth = require("../middleware/authAdmin");

const questionCtrl = require("../controllers/question");

router.post("/", auth, questionCtrl.create);
router.put("/:id", auth, questionCtrl.modify);
router.delete("/:id", auth, questionCtrl.delete);

module.exports = router;