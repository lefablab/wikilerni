const express = require("express");
const router = express.Router();

const auth = require("../middleware/authAdmin");

const choiceCtrl = require("../controllers/choice");

router.post("/", auth, choiceCtrl.create);
router.put("/:id", auth, choiceCtrl.modify);

module.exports = router;