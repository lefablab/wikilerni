const express = require("express");
const router = express.Router();

const auth = require("../middleware/auth");

const paymentCtrl = require("../controllers/payment");

// réception infos paiement venant du site de WebPortage
router.get("/WP-infos.html", paymentCtrl.saveUserPaymentInfos);
router.get("/getforoneuser/:id", auth, paymentCtrl.getOneUserPayments);

module.exports = router;