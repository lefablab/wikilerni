const express = require("express");
const router = express.Router();

const authAdmin = require("../middleware/authAdmin");
const auth = require("../middleware/auth");

const questionnaireCtrl = require("../controllers/questionnaire");
const questionnairesUserCtrl = require("../controllers/subscription");
const answerCtrl = require("../controllers/answer");

// Administrateurs du site ou créateurs de questionnaires
router.post("/", authAdmin, questionnaireCtrl.create);
router.put("/:id", authAdmin, questionnaireCtrl.modify);
router.delete("/:id", authAdmin, questionnaireCtrl.delete);
router.get("/getlistnextquestionnaires", authAdmin, questionnaireCtrl.getListNextQuestionnaires);
router.get("/htmlregenerated", authAdmin, questionnaireCtrl.HTMLRegenerate);

// Utilisateurs du site
router.post("/search", auth, questionnaireCtrl.searchQuestionnaires);
router.post("/getrandom", auth, questionnaireCtrl.getRandomQuestionnaires);
router.post("/searchadmin", authAdmin, questionnaireCtrl.searchAdminQuestionnaires);
router.get("/stats", auth, questionnaireCtrl.getStats);
router.get("/get/:id", questionnaireCtrl.getOneQuestionnaireById);
router.get("/preview/:id/:token", questionnaireCtrl.showOneQuestionnaireById);// prévisualisation HTML, même si questionnaire "incomplet"
router.post("/answer/", auth, answerCtrl.create);
router.get("/user/anwswers/stats/:userId", auth, answerCtrl.getStatsByUser);
router.get("/user/answers/:userId/:questionnaireId", auth, answerCtrl.getAnswersByQuestionnaire);

module.exports = router;