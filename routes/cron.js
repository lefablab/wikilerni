const express = require("express");
const router = express.Router();

const cronAuth = require("../middleware/cronAuth");

const ctrlUser = require("../controllers/user");
const ctrlPause = require("../controllers/pause");
const ctrlQuestionnaire = require("../controllers/questionnaire");
const ctrlGroup = require("../controllers/group");
const ctrlQuestion = require("../controllers/question");
const ctrlIllustration = require("../controllers/illustration");
const ctrlSubscription = require("../controllers/subscription");

// Users
router.get("/deleteloginfail/:token", cronAuth, ctrlUser.deleteLoginFail);
router.get("/deleteusersjson/:token", cronAuth, ctrlUser.deleteJsonFiles);
router.get("/deleteunvalided/:token", cronAuth, ctrlUser.deleteUnvalided);
router.get("/deleteinactiveaccounts/:token", cronAuth, ctrlUser.deleteInactiveAccounts);

// Pauses
router.get("/deleteoldpauses/:token", cronAuth, ctrlPause.deleteOldPauses);

// Subscriptions
router.get("/notifyexpirationfreeaccount/:token", cronAuth, ctrlSubscription.notifyExpirationFreeAccount);
router.get("/notifyexpirationaccount/:token", cronAuth, ctrlSubscription.notifyExpirationAccount);
router.get("/addquestionnairetouser/:token", cronAuth, ctrlSubscription.addNewQuestionnaireUsers);

// Questionnaires
router.get("/deletequestionnairesfiles/:token", cronAuth, ctrlQuestionnaire.deleteJsonFiles);
router.get("/deletequestionsfiles/:token", cronAuth, ctrlQuestion.deleteJsonFiles);
router.get("/publishquestionnaires/:token", cronAuth, ctrlQuestionnaire.checkQuestionnairesNeedToBePublished);
// + Groupes
router.get("/deletegroupsfiles/:token", cronAuth, ctrlGroup.cronDeleteJsonFiles);
router.get("/publishgroups/:token", cronAuth, ctrlGroup.cronCheckGroupsNeedToBePublished);

// Illustrations des questionnaires
router.get("/deleteoldillustrations/:token", cronAuth, ctrlIllustration.deleteOldFiles);

module.exports = router;