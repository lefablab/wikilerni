const express = require("express");
const router = express.Router();

const auth = require("../middleware/authAdmin");
const multer = require("../middleware/multer-config");

const illustrationCtrl = require("../controllers/illustration");

router.post("/", auth, multer, illustrationCtrl.create);// ! multer après auth pour éviter téléchargement si utilisateur non autorisé.
router.put("/:id", auth, multer, illustrationCtrl.modify);
router.delete("/:id", auth, illustrationCtrl.delete);
router.get("/:id", auth, illustrationCtrl.getOneIllustrationById);

module.exports = router;