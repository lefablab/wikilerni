const express = require("express");
const router = express.Router();

const auth = require("../middleware/authAdmin");

const linkCtrl = require("../controllers/link");

router.post("/", auth, linkCtrl.create);
router.put("/:id", auth, linkCtrl.modify);
router.delete("/:id", auth, linkCtrl.delete);
router.get("/:id", auth, linkCtrl.getOneById);

module.exports = router;