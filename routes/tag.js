const express = require("express");
const router = express.Router();

const authAdmin = require("../middleware/authAdmin");
const tagCtrl = require("../controllers/tag");

router.post("/tags/search/", authAdmin, tagCtrl.getTagsBeginningBy);
// deux routes différentes, car succède à la création comme à la mise à jour d'un questionnaire via next() :
router.post("/", authAdmin, tagCtrl.checkTags);
router.put("/:id", authAdmin, tagCtrl.checkTags);// :id = "id" du questionnaire ciblé
// idem pour la regénération du HTML :
router.get("/htmlregenerated", authAdmin, tagCtrl.HTMLRegenerate);

module.exports = router;