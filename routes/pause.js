const express = require("express");
const router = express.Router();

const auth = require("../middleware/auth");

const pauseCtrl = require("../controllers/pause");

router.post("/crea", auth, pauseCtrl.create);
router.put("/:id", auth, pauseCtrl.modify);
router.delete("/:id", auth, pauseCtrl.delete);

module.exports = router;