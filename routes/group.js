const express = require("express");
const router = express.Router();

const auth = require("../middleware/auth");
const authAdmin = require("../middleware/authAdmin");

const answerCtrl = require("../controllers/answer");
const groupCtrl = require("../controllers/group");

router.post("/search", authAdmin, groupCtrl.searchGroups);
router.post("/", authAdmin, groupCtrl.create);
router.get("/stats", authAdmin, groupCtrl.getStatsGroups);
router.put("/:id", authAdmin, groupCtrl.modify);
router.delete("/:id", authAdmin, groupCtrl.delete);
router.get("/get/:id", authAdmin, groupCtrl.getOneById);
router.get("/preview/:id/:token", groupCtrl.showOneGroupById);// prévisualisation HTML, même si groupe "incomplet"

router.post("/answer/", auth, answerCtrl.createInGroup);
router.get("/user/answers/:userId/:groupId", auth, answerCtrl.getAnswersByGroup);

module.exports = router;